package gui.overview;

import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import common.EffortDto;
import util.DateAndTimeHelper;

public class LowerDetailTable extends AbstractOverviewTable<EffortDto> {

	private static final long serialVersionUID = 1L;

	private static final String[] COLUMN_LABELS_IDENTIFIERS = { "Date", "Start", "Stop", "Pause",
			"\u03a3 [hh:mm]", "Status", "Comment" };

	public static final int DATE_INDEX = 0;
	public static final int START_INDEX = 1;
	public static final int STOP_INDEX = 2;
	public static final int PAUSE_INDEX = 3;
	public static final int SUM_INDEX = 4;
	public static final int STATUS_INDEX = 5;
	public static final int COMMENT_INDEX = 6;

	public void setColumnWidth() {
		TableColumn column = null;
		DefaultTableCellRenderer tableRenderer = new DefaultTableCellRenderer();
		tableRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < getColumnCount(); i++) {
			column = getColumnModel().getColumn(i);
			column.setCellRenderer(tableRenderer);
			if (i == 6) {
				column.setPreferredWidth(100);
			} else {
				column.setPreferredWidth(25);
			}
		}
	}

	protected Object[] getRowArray(EffortDto effort) {
		Object[] array = new Object[getColumnCount()];
		array[DATE_INDEX] = effort.getEffortDate().toString();
		array[START_INDEX] = effort.getStartTime().toString();
		array[STOP_INDEX] = effort.getEndTime().toString();
		array[PAUSE_INDEX] = effort.getPauseTime().toString();
		array[SUM_INDEX] = DateAndTimeHelper.getDurationStringMMtoHHMM(effort.getDuration());
		array[STATUS_INDEX] = effort.getOrderStatus();
		array[COMMENT_INDEX] = effort.getComment();
		return array;
	}

	@Override
	protected String[] getColumnIdentifiers() {
		return COLUMN_LABELS_IDENTIFIERS;
	}
}
