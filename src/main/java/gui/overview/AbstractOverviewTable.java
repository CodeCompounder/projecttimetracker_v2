package gui.overview;

import java.awt.Dimension;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;

public abstract class AbstractOverviewTable<T> extends JTable {

	private static final long serialVersionUID = 1L;
	
	DefaultTableModel tableModel = new DefaultTableModel();
    DefaultTableColumnModel tableColumnModel = new DefaultTableColumnModel();
	
    public AbstractOverviewTable() {
    	setModel(tableModel);
		setAutoCreateRowSorter(true);
		setColumnModel(new DefaultTableColumnModel());
		tableModel.setColumnIdentifiers(getColumnIdentifiers());
		setColumnWidth();
		setPreferredScrollableViewportSize(new Dimension((int) getPreferredScrollableViewportSize().getWidth(),
                (int) getPreferredSize().getHeight()));
    }

	protected abstract void setColumnWidth();

	protected abstract String[] getColumnIdentifiers();
	
	public void addContent(List<T> contentList) {
		for (T content : contentList) {
			tableModel.addRow(getRowArray(content));
		}	
	}

	protected abstract Object[] getRowArray(T content);
	
}
