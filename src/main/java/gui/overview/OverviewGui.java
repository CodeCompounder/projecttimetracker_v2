package gui.overview;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneLayout;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.jdatepicker.impl.JDatePickerImpl;

import common.EffortDto;
import common.GroupedEffortDto;
import common.MainOrderDto;
import common.OrderCategoryDto;
import common.OrderStatusDto;
import common.SubOrderDto;
import controller.ApplicationControllerInf;
import util.DateAndTimeHelper;
import util.PttJDatePickerPanel;

public class OverviewGui {

	private ApplicationControllerInf controller;
	private StatusChangeTableModelListener tableListener = new StatusChangeTableModelListener();
	private JComboBox<Object> statusComboBox = new JComboBox<>();
	private JFrame mainFrame = new JFrame("Time Overview");

	// NorthPanel
	private JPanel northPanel = new JPanel(new GridLayout(0, 3, 5, 5));
	private PttJDatePickerPanel datePickerPanelStart;
	private PttJDatePickerPanel datePickerPanelEnd;
	private JButton searchButton;
	private JList<Object> catList;
	private JList<Object> statList;

	// CenterPanel
	private JPanel centerPanel = new JPanel(new GridLayout(0, 1));
	private JPanel centerUpperPanel = new JPanel(new BorderLayout());
	private JPanel centerLowerPanel = new JPanel(new BorderLayout());
	private UpperOverviewTable upperTable = new UpperOverviewTable();
	private LowerDetailTable lowerTable = new LowerDetailTable();

	// SouthPanel
	private JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

	public OverviewGui(ApplicationControllerInf controller) {
		this.controller = controller;

		initMainFrame();
		initNorthPanel();
		initCenterPanel();
		initSouthPanel();

		initCategories();
		initStatus();
		initSearch();
		initUpperTable();
	}

	private void initCategories() {
		DefaultListModel<Object> catListModel = (DefaultListModel<Object>) catList.getModel();
		catListModel.removeAllElements();
		List<OrderCategoryDto> categoryList = controller.getAllCategories();
		catListModel.addElement("*all*");
		catListModel.addAll(categoryList);
		catList.setSelectedIndex(0);
	}

	private void initStatus() {
		DefaultListModel<Object> statListModel = (DefaultListModel<Object>) statList.getModel();
		statListModel.removeAllElements();
		List<OrderStatusDto> statusList = controller.getAllStatuses();
		statListModel.addElement("*all*");
		statListModel.addAll(statusList);
		statList.setSelectedIndex(0);
	}

	private void initSearch() {
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fillUpperTable();
			}
		});
	}

	private void fillUpperTable() {
		DefaultTableModel upperTableModel = (DefaultTableModel) upperTable.getModel();
		upperTableModel.setRowCount(0);

		List<GroupedEffortDto> groupedEffortList = getGroupedEffortsByDatesAndCategoryAndStatus();

		upperTable.getModel().removeTableModelListener(tableListener);
		upperTable.addContent(groupedEffortList);
		upperTable.getModel().addTableModelListener(tableListener);

		setBorderLabel("Group by Order/Suborder | Total time " + getTotalTime(groupedEffortList));
	}

	private List<GroupedEffortDto> getGroupedEffortsByDatesAndCategoryAndStatus() {

		LocalDate startDate = DateAndTimeHelper.getLocalDateFromDatePicker(getDatePickerStart());
		LocalDate endDate = DateAndTimeHelper.getLocalDateFromDatePicker(getDatePickerEnd());
		OrderCategoryDto category = catList.getSelectedValue().equals("*all*") ? null
				: (OrderCategoryDto) catList.getSelectedValue();
		OrderStatusDto status = statList.getSelectedValue().equals("*all*") ? null
				: (OrderStatusDto) statList.getSelectedValue();

		return controller.getGroupedEffortByDate(startDate, endDate, category, status);
	}

	private String getTotalTime(List<GroupedEffortDto> groupedEffortList) {
		int totalTime = 0;
		for (GroupedEffortDto effort : groupedEffortList) {
			totalTime = totalTime + effort.getSummedTime();
		}
		return DateAndTimeHelper.getDurationStringMMtoHHMM(totalTime);
	}

	private void initUpperTable() {
		addStatusComboBox();
		addMouseListenerForLowerTableCreation();
	}

	private void addMouseListenerForLowerTableCreation() {
		upperTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				DefaultTableModel lowerTableModel = (DefaultTableModel) lowerTable.getModel();
				lowerTableModel.setRowCount(0);

				int selectedRow = upperTable.getSelectedRow();
				int selectedRowInModel = upperTable.convertRowIndexToModel(selectedRow);

				GroupedEffortDto groupedEffort = (GroupedEffortDto) upperTable.getModel()
						.getValueAt(selectedRowInModel, UpperOverviewTable.GROUPED_EFFORT_INDEX);

				List<EffortDto> effortList = controller.getEfforts(groupedEffort);
				lowerTable.addContent(effortList);

				addBorderWithEffortInformation(groupedEffort);
			}

			private void addBorderWithEffortInformation(GroupedEffortDto groupedEffort) {

				MainOrderDto mainOrder = groupedEffort.getMainOrder();
				SubOrderDto subOrder = groupedEffort.getSubOrder();
				OrderStatusDto status = groupedEffort.getStatus();
				String title = String.format("Details (%s   ;   %s   ;   %s)", mainOrder, subOrder,
						status);

				centerLowerPanel.setBorder(BorderFactory.createCompoundBorder(
						BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder(title)));
			}
		});
	}

	private void addStatusComboBox() {
		for (int i = 1; i < statList.getModel().getSize(); i++) {
			statusComboBox.addItem(statList.getModel().getElementAt(i));
		}
		DefaultCellEditor statusEditor = new DefaultCellEditor(statusComboBox);
		upperTable.getColumnModel().getColumn(6).setCellEditor(statusEditor);
	}

	private void initMainFrame() {
		mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		mainFrame.setLayout(new BorderLayout());

		try {
			Image image = ImageIO.read(getClass().getResource("/icon.png"));
			mainFrame.setIconImage(image);
		} catch (IOException e) {
			System.out.println("Can't find logo.");
			e.printStackTrace();
		}

		mainFrame.add(northPanel, BorderLayout.NORTH);
		mainFrame.add(centerPanel, BorderLayout.CENTER);
		mainFrame.add(southPanel, BorderLayout.SOUTH);

		mainFrame.setSize(740, 600);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}

	private void initNorthPanel() {
		northPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JPanel northLeftPanel = new JPanel(new GridLayout(3, 0, 5, 5));
		northPanel.add(northLeftPanel);

		// Left Panel
		datePickerPanelStart = new PttJDatePickerPanel();
		datePickerPanelStart.getPickerPanel().setBorder(BorderFactory.createTitledBorder("Start"));
		northLeftPanel.add(datePickerPanelStart.getPickerPanel());

		datePickerPanelEnd = new PttJDatePickerPanel();
		datePickerPanelEnd.getPickerPanel().setBorder(BorderFactory.createTitledBorder("END"));
		northLeftPanel.add(datePickerPanelEnd.getPickerPanel());

		JPanel searchButtonPanel = new JPanel(new BorderLayout());
		searchButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 5));
		searchButton = new JButton("Search");
		searchButton.setFont(new Font(searchButton.getFont().getFontName(), 0, 20));
		searchButtonPanel.add(searchButton);
		northLeftPanel.add(searchButtonPanel);

		// Center Panel
		JPanel northCenterPanel = new JPanel(new BorderLayout());
		northCenterPanel.setBorder(BorderFactory.createTitledBorder("Select Categories"));
		DefaultListModel<Object> catListModel = new DefaultListModel<>();
		catList = new JList<Object>(catListModel);
		catList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane catScrollPane = new JScrollPane(catList);
		northCenterPanel.add(catScrollPane, BorderLayout.CENTER);
		northPanel.add(northCenterPanel);

		// Right Panel
		JPanel northRightPanel = new JPanel(new BorderLayout());
		northRightPanel.setBorder(BorderFactory.createTitledBorder("Select Status"));
		DefaultListModel<Object> statListModel = new DefaultListModel<>();
		statList = new JList<Object>(statListModel);
		statList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane statScrollPane = new JScrollPane(statList);
		northRightPanel.add(statScrollPane, BorderLayout.CENTER);
		northPanel.add(northRightPanel);

	}

	private void initCenterPanel() {
		centerUpperPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder("Group by Order/Suborder")));
		centerPanel.add(centerUpperPanel);

		centerLowerPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder("Details")));
		centerPanel.add(centerLowerPanel);

		// Upper Panel
		JScrollPane upperScrollPane = new JScrollPane(upperTable);
		upperScrollPane.setLayout(new ScrollPaneLayout());
		centerUpperPanel.add(upperScrollPane, BorderLayout.CENTER);

		// Lower Panel
		JScrollPane lowerScrollPane = new JScrollPane(lowerTable);
		lowerScrollPane.setLayout(new ScrollPaneLayout());
		centerLowerPanel.add(lowerScrollPane, BorderLayout.CENTER);
	}

	private void initSouthPanel() {
		JButton closeButton = new JButton("Close");
		southPanel.add(closeButton);
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mainFrame.dispose();
			}
		});
	}

	public void setBorderLabel(String label) {
		centerUpperPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder(label)));
	}

	// GETTER
	public JFrame getMainFrame() {
		return mainFrame;
	}

	public JDatePickerImpl getDatePickerStart() {
		return datePickerPanelStart.getDatePicker();
	}

	public JDatePickerImpl getDatePickerEnd() {
		return datePickerPanelEnd.getDatePicker();
	}

	public JButton getSearchButton() {
		return searchButton;
	}

	public JList<Object> getCatList() {
		return catList;
	}

	public JList<Object> getStatList() {
		return statList;
	}

	// CenterPanel
	public UpperOverviewTable getUpperTable() {
		return upperTable;
	}

	public LowerDetailTable getLowerTable() {
		return lowerTable;
	}

	public JPanel getCenterLowerPanel() {
		return centerLowerPanel;
	}

	private class StatusChangeTableModelListener implements TableModelListener {
		@Override
		public void tableChanged(TableModelEvent e) {
			if (e.getColumn() != UpperOverviewTable.STATUS_INDEX) {
				return;
			}

			DefaultTableModel model = (DefaultTableModel) e.getSource();
			GroupedEffortDto groupedEffort = (GroupedEffortDto) model.getValueAt(e.getFirstRow(),
					UpperOverviewTable.GROUPED_EFFORT_INDEX);
			List<EffortDto> effortList = controller.getEfforts(groupedEffort);

			OrderStatusDto statusNew = (OrderStatusDto) statusComboBox.getSelectedItem();

			for (EffortDto effort : effortList) {
				effort.setOrderStatus(statusNew);
				controller.modifyEffort(effort);
				fillUpperTable();
			}
		}
	}

}
