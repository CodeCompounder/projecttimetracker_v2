package gui.overview;

import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import common.GroupedEffortDto;
import util.DateAndTimeHelper;

public class UpperOverviewTable extends AbstractOverviewTable<GroupedEffortDto> {

	private static final long serialVersionUID = 1L;

	private static final String[] COLUMN_LABELS_IDENTIFIERS = { "\u03a3 [hh:mm]", "OrderNr",
			"Project", "Category", "SubOrder", "SubProject", "Status", "ReferenceToGroupedEffort" };

	public static final int SUMMED_TIME_INDEX = 0;
	public static final int ORDER_NR_INDEX = 1;
	public static final int ORDER_NAME_INDEX = 2;
	public static final int CATEGORY_INDEX = 3;
	public static final int SUBORDER_NR_INDEX = 4;
	public static final int SUBORDER_NAME_INDEX = 5;
	public static final int STATUS_INDEX = 6;
	public static final int GROUPED_EFFORT_INDEX = 7;

	public void setColumnWidth() {
		TableColumn column = null;
		DefaultTableCellRenderer tableRenderer = new DefaultTableCellRenderer();
		tableRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < getColumnCount(); i++) {
			column = getColumnModel().getColumn(i);
			column.setCellRenderer(tableRenderer);
			if (i == 2 || i == 5) {
				column.setPreferredWidth(100);
			} else if (i == 0 || i == 1) {
				column.setPreferredWidth(25);
			} else if (i == 7) {
				column.setMinWidth(0);
				column.setMaxWidth(0);
			} else {
				column.setPreferredWidth(50);
			}
		}
	}

	protected Object[] getRowArray(GroupedEffortDto groupedEffort) {
		Object[] array = new Object[getColumnCount()];
		array[SUMMED_TIME_INDEX] = DateAndTimeHelper
				.getDurationStringMMtoHHMM(groupedEffort.getSummedTime());
		array[ORDER_NR_INDEX] = groupedEffort.getMainOrder().getMainOrderNumber();
		array[ORDER_NAME_INDEX] = groupedEffort.getMainOrder().getName();
		array[GROUPED_EFFORT_INDEX] = groupedEffort;

		if (groupedEffort.getMainOrder().getOrderCategory() != null) {
			array[CATEGORY_INDEX] = groupedEffort.getMainOrder().getOrderCategory();
		}

		if (groupedEffort.getSubOrder() != null) {
			array[SUBORDER_NR_INDEX] = groupedEffort.getSubOrder().getSubOrderNumber();
			array[SUBORDER_NAME_INDEX] = groupedEffort.getSubOrder().getName();
		}

		if (groupedEffort.getStatus() != null) {
			array[STATUS_INDEX] = groupedEffort.getStatus().getName();
		}

		return array;
	}

	@Override
	protected String[] getColumnIdentifiers() {
		return COLUMN_LABELS_IDENTIFIERS;
	}

}
