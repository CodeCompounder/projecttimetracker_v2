package gui.projects;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import common.MainOrderDto;
import common.SubOrderDto;
import controller.ApplicationControllerInf;
import gui.projects.elementgroups.MainOrderElementGroup;
import gui.projects.elementgroups.SubOrderElementGroup;

public class ProjectAdministrationPanel {

	private JFrame mainFrame;
	private JPanel projectPanel = new JPanel();
	private JPanel northPanel = new JPanel();
	private JPanel centerPanel = new JPanel();
	private JPanel southPanel = new JPanel();
	private JTextField searchProjectTextField;
	private JCheckBox visibleBox = new JCheckBox("Show only visible?", true);
	private MainOrderElementGroup mainOrderGroup;
	private SubOrderElementGroup subOrderGroup;
	private ApplicationControllerInf controller;

	public ProjectAdministrationPanel(JFrame mainFrame, ApplicationControllerInf controller) {
		this.mainFrame = mainFrame;
		this.controller = controller;

		this.mainOrderGroup = new MainOrderElementGroup(controller);
		this.subOrderGroup = new SubOrderElementGroup(mainOrderGroup, controller);

		init();
	}

	private void init() {
		projectPanel.setLayout(new BorderLayout());
		projectPanel.add(northPanel, BorderLayout.NORTH);
		projectPanel.add(centerPanel, BorderLayout.CENTER);
		projectPanel.add(southPanel, BorderLayout.SOUTH);

		initNorthPanel();
		initCenterPanel();
		initSouthPanel();

		addSearchTextKeyListener();
		addVisibleBoxItemListener();

		addMainOrderListListener();
		addSubOrderListListener();
	}

	private void initSouthPanel() {
		southPanel.setLayout(new GridLayout(0, 2));

		JPanel lowerLeftPanel = new JPanel();
		southPanel.add(lowerLeftPanel);
		lowerLeftPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		JPanel lowerRightPanel = new JPanel();
		southPanel.add(lowerRightPanel);
		lowerRightPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.dispose();
			}
		});
		lowerRightPanel.add(closeButton);
	}

	private void initCenterPanel() {
		centerPanel.setLayout(new BorderLayout());

		JPanel middleLowerPanel = new JPanel();
		centerPanel.add(middleLowerPanel, BorderLayout.SOUTH);
		middleLowerPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		middleLowerPanel.add(visibleBox);

		// Order List
		JPanel middleCenterPanel = new JPanel();
		centerPanel.add(middleCenterPanel, BorderLayout.CENTER);
		middleCenterPanel.setLayout(new GridLayout(0, 2, 5, 5));

		JList<MainOrderDto> orderList = mainOrderGroup.getEntityListBox();
		orderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane orderScrollPane = new JScrollPane(orderList);
		middleCenterPanel.add(orderScrollPane);

		// SubOrder List
		JList<SubOrderDto> subOrderList = subOrderGroup.getEntityListBox();
		subOrderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane subOrderScrollPane = new JScrollPane(subOrderList);
		middleCenterPanel.add(subOrderScrollPane);
	}

	private void initNorthPanel() {
		northPanel.setLayout(new BorderLayout());
		northPanel.setBorder(BorderFactory.createEtchedBorder());

		JPanel upperPanel = new JPanel();
		upperPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
		northPanel.add(upperPanel, BorderLayout.NORTH);
		upperPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		JLabel searchProjectLabel = new JLabel("Search Order: ");
		upperPanel.add(searchProjectLabel);
		searchProjectTextField = new JTextField("", 15);
		upperPanel.add(searchProjectTextField);

		JPanel upperCenterPanel = new JPanel();
		upperCenterPanel.setLayout(new GridLayout(0, 2, 5, 5));
		northPanel.add(upperCenterPanel, BorderLayout.CENTER);

		JPanel upperCenterWestPanel = new JPanel();
		upperCenterPanel.add(upperCenterWestPanel);
		upperCenterWestPanel.setBorder(BorderFactory.createTitledBorder("Order"));
		upperCenterWestPanel.setLayout(new BorderLayout());
		JPanel upperCenterWestNorthPanel = new JPanel();
		upperCenterWestNorthPanel.setLayout(new GridLayout(0, 2));
		upperCenterWestPanel.add(upperCenterWestNorthPanel, BorderLayout.NORTH);
		upperCenterWestNorthPanel.add(new JLabel("No.:"));
		upperCenterWestNorthPanel.add(mainOrderGroup.getOrderNrTextField());
		upperCenterWestNorthPanel.add(new JLabel("Description:            "));
		upperCenterWestNorthPanel.add(mainOrderGroup.getOrderNameTextField());
		upperCenterWestNorthPanel.add(new JLabel("Category"));
		upperCenterWestNorthPanel.add(mainOrderGroup.getCategoryComboBox());
		upperCenterWestNorthPanel.add(new JLabel(""));
		upperCenterWestNorthPanel.add(mainOrderGroup.getOrderVisible());

		JPanel upperCenterWestLowerPanel = new JPanel();
		upperCenterWestLowerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		upperCenterWestPanel.add(upperCenterWestLowerPanel, BorderLayout.SOUTH);
		upperCenterWestLowerPanel.add(mainOrderGroup.getNewButton());
		upperCenterWestLowerPanel.add(mainOrderGroup.getModifyButton());
		upperCenterWestLowerPanel.add(mainOrderGroup.getDeleteButton());

		// Suborder Elements
		JPanel upperCenterEastPanel = new JPanel();
		upperCenterPanel.add(upperCenterEastPanel);
		upperCenterEastPanel.setBorder(BorderFactory.createTitledBorder("Suborder"));
		upperCenterEastPanel.setLayout(new BorderLayout());
		JPanel upperCenterEastUpperPanel = new JPanel();
		upperCenterEastUpperPanel.setLayout(new GridLayout(0, 2));
		upperCenterEastPanel.add(upperCenterEastUpperPanel, BorderLayout.NORTH);
		upperCenterEastUpperPanel.add(new JLabel("No. "));
		upperCenterEastUpperPanel.add(subOrderGroup.getSubOrderNrTextField());
		upperCenterEastUpperPanel.add(new JLabel("Description:            "));
		upperCenterEastUpperPanel.add(subOrderGroup.getSubOrderNameTextField());
		upperCenterEastUpperPanel.add(new JLabel(""));
		upperCenterEastUpperPanel.add(new JLabel(""));
		upperCenterEastUpperPanel.add(new JLabel(""));
		upperCenterEastUpperPanel.add(subOrderGroup.getSubOrderVisible());

		JPanel upperCenterEastLowerPanel = new JPanel();
		upperCenterEastLowerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		upperCenterEastPanel.add(upperCenterEastLowerPanel, BorderLayout.SOUTH);
		upperCenterEastLowerPanel.add(subOrderGroup.getNewButton());
		upperCenterEastLowerPanel.add(subOrderGroup.getModifyButton());
		upperCenterEastLowerPanel.add(subOrderGroup.getDeleteButton());
	}

	private void addMainOrderListListener() {
		JList<MainOrderDto> orderListBox = mainOrderGroup.getEntityListBox();
		orderListBox.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting())
					// Prevent from invoking this method 2 times:
					return;
				MainOrderDto selectedOrder = (MainOrderDto) orderListBox.getSelectedValue();

				mainOrderGroup.getOrderNameTextField().setText(selectedOrder.getName());
				mainOrderGroup.getOrderNrTextField().setText(selectedOrder.getMainOrderNumber());
				mainOrderGroup.getOrderVisible().setSelected(selectedOrder.isVisible());
				mainOrderGroup.getCategoryComboBox()
						.setSelectedItem(selectedOrder.getOrderCategory());

				subOrderGroup.getListModel().removeAllElements();
				List<SubOrderDto> subOrderList = controller.getSubOrders(selectedOrder,
						subOrderGroup.getSubOrderVisible().isSelected());
				subOrderList.sort(Comparator.comparing(SubOrderDto::getSubOrderNumber));
				subOrderGroup.getListModel().addAll(subOrderList);
			}
		});
	}

	private void addSubOrderListListener() {
		JList<SubOrderDto> subOrderListBox = subOrderGroup.getEntityListBox();
		subOrderGroup.getEntityListBox().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting())
					// Prevent from invoking this method 2 times:
					return;
				SubOrderDto selectedSubOrder = (SubOrderDto) subOrderListBox.getSelectedValue();

				subOrderGroup.getSubOrderNameTextField().setText(selectedSubOrder.getName());
				subOrderGroup.getSubOrderNrTextField()
						.setText(selectedSubOrder.getSubOrderNumber());
				subOrderGroup.getSubOrderVisible().setSelected(selectedSubOrder.isVisible());
			}
		});
	}

	private void addVisibleBoxItemListener() {
		visibleBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				Robot keyTrigger;
				// -> Invoke KeyEvent of search field (17 = STRG-Taste)
				try {
					keyTrigger = new Robot();
					searchProjectTextField.requestFocus();
					keyTrigger.keyRelease(17);
				} catch (AWTException e1) {
					e1.printStackTrace();
				}
				subOrderGroup.getListModel().removeAllElements();
			}
		});

	}

	private void addSearchTextKeyListener() {
		searchProjectTextField.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				mainOrderGroup.getListModelOrder().removeAllElements();
				mainOrderGroup.fillEntityList(controller.getMainOrdersByName(
						searchProjectTextField.getText(), visibleBox.isSelected()));
			}
		});
	}

	public JPanel getMainPanel() {
		return projectPanel;
	}

	public void fillCategories() {
		mainOrderGroup.fillCategories();
	}
}
