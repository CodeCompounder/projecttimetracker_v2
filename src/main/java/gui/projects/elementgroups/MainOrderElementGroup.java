package gui.projects.elementgroups;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import common.MainOrderDto;
import controller.ApplicationControllerInf;


public class MainOrderElementGroup extends AbstractChangeableElementGroup<MainOrderDto> {

    private JTextField orderNrTextField = new JTextField("");
    private JTextField orderNameTextField = new JTextField("");
    private JComboBox<String> categoryComboBox = new JComboBox<String>();
    private JCheckBox orderVisible = new JCheckBox("visible?", true);
    
	public MainOrderElementGroup(ApplicationControllerInf controller) {
		super(controller);
		fillCategories();
		fillMainOrders();
		
		addVisibleBoxListener();
	}

	private void addVisibleBoxListener() {
		orderVisible.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				getSelectedEntity().setVisible(orderVisible.isSelected());				
			}			
		});		
	}

	private void fillMainOrders() {
		fillEntityList(controller.getAllMainOrders(orderVisible.isSelected()));	
	}

	public void fillCategories() {
		categoryComboBox.removeAllItems();
		controller.getAllCategories().stream().forEach(c -> categoryComboBox.addItem(c.getName()));
	}

	@Override
	public MainOrderDto getNewEntity() {
		String orderNr = orderNrTextField.getText();
		String description = orderNameTextField.getText();
		String category = (String) categoryComboBox.getSelectedItem();
		boolean visible = orderVisible.isSelected();
		
		return new MainOrderDto.Builder()
				.withMainOrderName(description)
				.withMainOrderNumber(orderNr)
				.withOrderCategery(category)
				.withVisible(visible)
				.build();
	}

	@Override
	public MainOrderDto getSelectedEntity() {
		return (MainOrderDto) entityListBox.getSelectedValue();
	}

	@Override
	protected void resetFields() {
		orderNameTextField.setText("");
        orderNrTextField.setText("");
        categoryComboBox.setSelectedIndex(-1);	
	}

	public JTextField getOrderNrTextField() {
		return orderNrTextField;
	}

	public JTextField getOrderNameTextField() {
		return orderNameTextField;
	}

	public JComboBox<String> getCategoryComboBox() {
		return categoryComboBox;
	}

	public JCheckBox getOrderVisible() {
		return orderVisible;
	}

	public DefaultListModel<MainOrderDto> getListModelOrder() {
		return listModel;
	}

	@Override
	protected void persistEntity(MainOrderDto newEntity) {
		controller.createMainOrder(newEntity);
	}

	@Override
	protected void modifyEntity(MainOrderDto selectedEntity) {
		controller.modifyMainOrder(selectedEntity);		
	}
}
