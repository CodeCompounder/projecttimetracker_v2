package gui.projects.elementgroups;

import javax.swing.JTextField;

import common.OrderStatusDto;
import controller.ApplicationControllerInf;

public class OrderStatusElementGroup extends AbstractChangeableElementGroup<OrderStatusDto> {

	private JTextField statusNameTextField = new JTextField("", 10);
	
	public OrderStatusElementGroup(ApplicationControllerInf controller) {
		super(controller);
	}

	@Override
	public OrderStatusDto getNewEntity() {
		OrderStatusDto statusDto = new OrderStatusDto();
		statusDto.setName(statusNameTextField.getText());
		return statusDto;
	}

	@Override
	public OrderStatusDto getSelectedEntity() {
		return (OrderStatusDto) entityListBox.getSelectedValue();
	}

	@Override
	protected void resetFields() {
		statusNameTextField.setText("");		
	}
	
	public JTextField getStatusNameTextField() {
		return statusNameTextField;
	}

	@Override
	protected void persistEntity(OrderStatusDto newEntity) {
		controller.createOrderStatus(newEntity.getName());		
	}
	
	@Override
	protected void modifyEntity(OrderStatusDto selectedEntity) {
		controller.modifyOrderStatus(selectedEntity);		
	}

}
