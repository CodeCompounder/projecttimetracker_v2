package gui.projects.elementgroups;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import common.SubOrderDto;
import controller.ApplicationControllerInf;

public class SubOrderElementGroup extends AbstractChangeableElementGroup<SubOrderDto> {

    private JTextField subOrderNrTextField = new JTextField("");
    private JTextField subOrderNameTextField = new JTextField("");
    private JCheckBox subOrderVisible = new JCheckBox("visible?", true);    
    private MainOrderElementGroup mainOrderGroup;
	
	public SubOrderElementGroup(MainOrderElementGroup mainOrderGroup, ApplicationControllerInf controller) {
		super(controller);
		this.mainOrderGroup = mainOrderGroup;
		
		addVisibleBoxListener();
	}	
	
	private void addVisibleBoxListener() {
		subOrderVisible.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				getSelectedEntity().setVisible(subOrderVisible.isSelected());				
			}			
		});		
	}
	
	@Override
	public SubOrderDto getNewEntity() {
		String subDescription = subOrderNameTextField.getText();
		String subOrderNr = subOrderNrTextField.getText();
		boolean visible = subOrderVisible.isSelected();
		
		return new SubOrderDto.Builder()
				.withMainOrderId(mainOrderGroup.getSelectedEntity().getId())
				.withSubOrderName(subDescription)
				.withSubOrderNumber(subOrderNr)
				.withVisible(visible)
				.build();
	}

	@Override
	public SubOrderDto getSelectedEntity() {
		return (SubOrderDto) entityListBox.getSelectedValue();
	}

	@Override
	protected void resetFields() {
		subOrderNameTextField.setText("");
        subOrderNrTextField.setText("");		
	}

	public JTextField getSubOrderNrTextField() {
		return subOrderNrTextField;
	}

	public JTextField getSubOrderNameTextField() {
		return subOrderNameTextField;
	}

	public JCheckBox getSubOrderVisible() {
		return subOrderVisible;
	}

	public void setMainOrderGroup(MainOrderElementGroup mainOrderGroup) {
		this.mainOrderGroup = mainOrderGroup;
	}

	@Override
	protected void persistEntity(SubOrderDto newEntity) {
		controller.createSubOrder(newEntity);
	}

	@Override
	protected void modifyEntity(SubOrderDto selectedEntity) {
		controller.modifySubOrder(selectedEntity);
	}
}
