package gui.projects.elementgroups;

import javax.swing.JTextField;

import common.OrderCategoryDto;
import controller.ApplicationControllerInf;

public class OrderCategoryElementGroup extends AbstractChangeableElementGroup<OrderCategoryDto> {

	private JTextField catNameTextField = new JTextField("", 10);
	
	public OrderCategoryElementGroup(ApplicationControllerInf controller) {
		super(controller);
	}

	@Override
	public OrderCategoryDto getNewEntity() {
		OrderCategoryDto categeroyDto = new OrderCategoryDto();
		categeroyDto.setName(catNameTextField.getText());
		return categeroyDto;
	}

	@Override
	public OrderCategoryDto getSelectedEntity() {
		return (OrderCategoryDto) entityListBox.getSelectedValue();
	}

	@Override
	protected void resetFields() {
		catNameTextField.setText("");		
	}

	public JTextField getCatNameTextField() {
		return catNameTextField;
	}

	@Override
	protected void persistEntity(OrderCategoryDto newEntity) {
		controller.createCategory(newEntity.getName());		
	}

	@Override
	protected void modifyEntity(OrderCategoryDto selectedEntity) {
		controller.modifyCategory(selectedEntity);
	}
}
