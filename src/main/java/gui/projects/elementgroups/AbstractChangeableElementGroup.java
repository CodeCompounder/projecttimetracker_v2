package gui.projects.elementgroups;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Comparator;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;

import org.hibernate.exception.ConstraintViolationException;

import common.ProjectTimeTrackerDto;
import controller.ApplicationControllerInf;
import jakarta.persistence.PersistenceException;

public abstract class AbstractChangeableElementGroup<T extends ProjectTimeTrackerDto> {

	protected ApplicationControllerInf controller;
	protected JButton newButton = new JButton("New");
	protected JButton modifyButton = new JButton("Modify");
	protected JButton deleteButton = new JButton("Delete");
	protected DefaultListModel<T> listModel = new DefaultListModel<T>();
	protected JList<T> entityListBox = new JList<>(listModel);

	public AbstractChangeableElementGroup(ApplicationControllerInf controller) {
		this.controller = controller;
		addButtonActionListener();
	}

	public abstract T getNewEntity();

	public abstract T getSelectedEntity();

	protected abstract void resetFields();

	public JButton getNewButton() {
		return newButton;
	}

	public JButton getModifyButton() {
		return modifyButton;
	}

	public JButton getDeleteButton() {
		return deleteButton;
	}

	public JList<T> getEntityListBox() {
		return entityListBox;
	}

	@SuppressWarnings("unchecked")
	public void fillEntityList(List<T> entityList) {
		listModel.removeAllElements();
		entityList.sort((Comparator<? super T>) Comparator.naturalOrder());
		listModel.addAll(entityList);
	}

	public DefaultListModel<T> getListModel() {
		return listModel;
	}

	@SuppressWarnings("unchecked")
	private void addToEntityList(Object newEntity) {
		T castedNewEntity = (T) newEntity;
		listModel.add(0, castedNewEntity);
	}

	private boolean isUnique(Object entity) {
		for (int i = 0; i < listModel.size(); i++) {
			Object currentObject = listModel.get(i);
			if (currentObject.toString().equals(entity.toString())) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean hasChanged(Object entity) {
		T castedentity = (T) entity;
		T newEntity = getNewEntity();
		return (!newEntity.equals(castedentity));
	}

	private void addButtonActionListener() {
		newButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				T newEntity = getNewEntity();
				if (!isUnique(newEntity)) {
					JOptionPane.showMessageDialog(newButton, "Entry already exists!");
					return;
				}
				persistEntity(newEntity);
				addToEntityList(newEntity);
			}
		});

		modifyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				T selectedEntity = getSelectedEntity();
				if (selectedEntity == null) {
					JOptionPane.showMessageDialog(modifyButton, "Select element!");
					return;
				}
				if (!hasChanged(selectedEntity)) {
					JOptionPane.showMessageDialog(modifyButton, "There are no changes!");
					return;
				}
				;
				T modifiedEntity = getNewEntity();
				modifiedEntity.setId(selectedEntity.getId());
				modifyEntity(modifiedEntity);

				int selectedIndex = entityListBox.getSelectedIndex();
				listModel.removeElementAt(selectedIndex);
				listModel.add(selectedIndex, modifiedEntity);
				entityListBox.setModel(listModel);
			}
		});

		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = JOptionPane.showConfirmDialog(deleteButton,
						"Delete '" + getSelectedEntity() + "'?", "Delete",
						JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if (i == 1) {
					return;
				}
				try {
					controller.deleteEntity(getSelectedEntity());
					listModel.removeElement(getSelectedEntity());
				} catch (PersistenceException f) {
					handleDeleteException(f);
				}
			}

			private void handleDeleteException(PersistenceException f) {
				if (f.getCause() instanceof ConstraintViolationException) {
					JOptionPane.showMessageDialog(deleteButton,
							"Entry cannot be deleted: Subordinate record(s) found! These must be deleted first.");
					return;
				}
				f.printStackTrace();
			}
		});
	}

	protected abstract void modifyEntity(T selectedEntity);

	protected abstract void persistEntity(T newEntity);
}
