package gui.projects;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import common.OrderCategoryDto;
import common.OrderStatusDto;
import common.ProjectTimeTrackerDto;
import controller.ApplicationControllerInf;
import gui.projects.elementgroups.AbstractChangeableElementGroup;
import gui.projects.elementgroups.OrderCategoryElementGroup;
import gui.projects.elementgroups.OrderStatusElementGroup;

public class CategoryAndStatusPanel {

	private ApplicationControllerInf controller;
	private JFrame mainFrame;
	private JPanel mainPanel = new JPanel();
	private JPanel categoriesPanel = new JPanel();
	private JPanel statusPanel = new JPanel();
	private JPanel closeButtonPanel = new JPanel();
    private OrderCategoryElementGroup categoryGroup;
    private OrderStatusElementGroup statusGroup;
    
	public CategoryAndStatusPanel (JFrame mainFrame, ApplicationControllerInf controller) {
		this.mainFrame = mainFrame;
		this.controller = controller;
		
		this.categoryGroup = new OrderCategoryElementGroup(controller);
		this.statusGroup = new OrderStatusElementGroup(controller);
		
		init();
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	private void init() {
		mainPanel.setLayout(new BorderLayout());
        mainPanel.add(categoriesPanel, BorderLayout.NORTH);
        mainPanel.add(statusPanel, BorderLayout.CENTER);
        mainPanel.add(closeButtonPanel, BorderLayout.SOUTH);
        
        initCategoryPanel();
        initStatusPanel();
        initCloseButtonPanel();
        
		fillCategoryList();
		fillStatusList();
		
		initElementGroupListener(categoryGroup, categoryGroup.getCatNameTextField());
		initElementGroupListener(statusGroup, statusGroup.getStatusNameTextField());
	}
	
	private <T extends ProjectTimeTrackerDto> void initElementGroupListener(AbstractChangeableElementGroup<T> elementGroup, JTextField aTextField) {
		elementGroup.getEntityListBox().addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting())
					return;
				aTextField.setText(
						elementGroup.getEntityListBox().getSelectedValue().getName());
			}
		});		
	}

	private void fillStatusList() {
		List<OrderStatusDto> statusList = controller.getAllStatuses();
		statusGroup.getListModel().removeAllElements();
		statusGroup.getListModel().addAll(statusList);
	}

	private void fillCategoryList() {
		List<OrderCategoryDto> categoryList = controller.getAllCategories();
		categoryGroup.getListModel().removeAllElements();
		categoryGroup.getListModel().addAll(categoryList);
	}

	private void initCategoryPanel() {
		categoriesPanel.setLayout(new GridLayout(0,2,5,5));
		categoriesPanel.setBorder(BorderFactory.createTitledBorder("Categories"));;
        
        //Categories left side
        JPanel categoriesLeftSidePanel = new JPanel();
        categoriesLeftSidePanel.setLayout(new BorderLayout());
        categoriesPanel.add(categoriesLeftSidePanel);
        
        JPanel categoriesInputPanel = new JPanel();
        categoriesInputPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        categoriesLeftSidePanel.add(categoriesInputPanel, BorderLayout.NORTH);
        categoriesInputPanel.add(new JLabel("Name:   "));       
        categoriesInputPanel.add(categoryGroup.getCatNameTextField());
        
        JPanel categoriesButtonGroupPanel = new JPanel();
        categoriesButtonGroupPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        categoriesLeftSidePanel.add(categoriesButtonGroupPanel, BorderLayout.CENTER);
        categoriesButtonGroupPanel.add(categoryGroup.getNewButton());
        categoriesButtonGroupPanel.add(categoryGroup.getModifyButton());
        categoriesButtonGroupPanel.add(categoryGroup.getDeleteButton());
        
        //Categories right side (list)
        JPanel categoriesListPanel = new JPanel();
        categoriesListPanel.setLayout(new BorderLayout());
        categoriesPanel.add(categoriesListPanel);
        
        categoryGroup.getEntityListBox().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane orderScrollPane = new JScrollPane(categoryGroup.getEntityListBox());
        categoriesListPanel.add(orderScrollPane, BorderLayout.CENTER);  
	}

	private void initStatusPanel() {
		statusPanel.setLayout(new GridLayout(0,2,5,5));
		statusPanel.setBorder(BorderFactory.createTitledBorder("Status"));;
        
        //Status left side
        JPanel statPanelLinks = new JPanel();
        statPanelLinks.setLayout(new BorderLayout());
        statusPanel.add(statPanelLinks);
        
        JPanel statPanelInputField = new JPanel();
        statPanelLinks.add(statPanelInputField, BorderLayout.NORTH);
        statPanelInputField.add(new JLabel("Name:   "));       
        statPanelInputField.add(statusGroup.getStatusNameTextField());
        
        JPanel statPanelObenButtonField = new JPanel();
        statPanelObenButtonField.setLayout(new FlowLayout(FlowLayout.LEFT));
        statPanelLinks.add(statPanelObenButtonField, BorderLayout.CENTER);
        statPanelObenButtonField.add(statusGroup.getNewButton());
        statPanelObenButtonField.add(statusGroup.getModifyButton());
        statPanelObenButtonField.add(statusGroup.getDeleteButton());
        
        JTextArea hintStatus = new JTextArea("Hint: Top entry is \n"
                + "the default setting!", 2,1);
        hintStatus.setBackground(null);
        hintStatus.setFont(new Font(hintStatus.getFont().getFontName(), 1, 13));
        hintStatus.setLineWrap(true);
        statPanelLinks.add(hintStatus, BorderLayout.SOUTH);
        
        
        //Status right side (Liste)
        JPanel statPanelRechts = new JPanel();
        statPanelRechts.setLayout(new BorderLayout());
        statusPanel.add(statPanelRechts);
        
        statusGroup.getEntityListBox().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane statScrollPane = new JScrollPane(statusGroup.getEntityListBox());
        statPanelRechts.add(statScrollPane, BorderLayout.CENTER);     
	}

	private void initCloseButtonPanel() {
		closeButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		closeButtonPanel.setBorder(BorderFactory.createEmptyBorder(50, 0, 0, 1));
        
		JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainFrame.dispose();         
            }            
        });
        
        closeButtonPanel.add(closeButton);
	}
}
