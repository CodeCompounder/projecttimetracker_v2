package gui.projects;

import java.awt.BorderLayout;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.ApplicationControllerInf;

public class ProjectsGui {

	private JFrame mainFrame = new JFrame("Projects/Categories/Status");
	private JTabbedPane centerPane = new JTabbedPane();
	private CategoryAndStatusPanel categoryAndStatusPanel;
	private ProjectAdministrationPanel projectPanel;

	public ProjectsGui(ApplicationControllerInf controller) {
		categoryAndStatusPanel = new CategoryAndStatusPanel(mainFrame, controller);
		projectPanel = new ProjectAdministrationPanel(mainFrame, controller);
		initMainFrame();
		addTabActionListener();
	}

	private void initMainFrame() {
		mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.add(centerPane, BorderLayout.CENTER);

		try {
			Image image = ImageIO.read(getClass().getResource("/icon.png"));
			mainFrame.setIconImage(image);
		} catch (IOException e) {
			System.out.println("Can't find logo.");
			e.printStackTrace();
		}

		centerPane.addTab("Projects", projectPanel.getMainPanel());
		centerPane.addTab("Categories/Status", categoryAndStatusPanel.getMainPanel());

		mainFrame.setSize(500, 500);
		mainFrame.setLocationRelativeTo(null);

		mainFrame.setVisible(true);
	}

	public JFrame getMainFrame() {
		return mainFrame;
	}

	private void addTabActionListener() {
		centerPane.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				projectPanel.fillCategories();
			}
		});
	}
}
