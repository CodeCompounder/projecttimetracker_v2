package gui.recorder;

import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import common.MainOrderDto;
import common.SubOrderDto;
import util.SteppedComboBox;

public class EffortTable extends JTable {

	DefaultTableModel tableModel = new DefaultTableModel();
	DefaultTableColumnModel tableColumnModel = new DefaultTableColumnModel();
	private SteppedComboBox<MainOrderDto> editOrderBox = new SteppedComboBox<>();
	private SteppedComboBox<SubOrderDto> editSubOrderBox = new SteppedComboBox<>();

	private static final long serialVersionUID = 1L;

	private static final String[] COLUMN_LABELS_OVERVIEW_STATE = { "ID", "Start", "Stop", "Pause",
			"\u03a3 [min]", "OrderNr", "Project", "Category", "SubOrder", "SubProject", "Status",
			"Comment" };

	private static final String[] REMOVED_COLUMNS_IN_MODIFY_STATE = { "\u03a3 [min]", "Project",
			"Category", "SubProject", "Status" };

	public EffortTable() {
		setModel(tableModel);
		setColumnModel(new DefaultTableColumnModel());
		setColumnLabels();
		setColumnWidth();
	}

	public void addTableOrderComboBox(List<MainOrderDto> orderList,
			CellEditorListener editOrderBoxListener) {
		editOrderBox.removeAllItems();
		editOrderBox.setModel(
				new DefaultComboBoxModel<MainOrderDto>(orderList.toArray(new MainOrderDto[0])));
		editOrderBox.setPopupWidth(250);
		DefaultCellEditor orderEditor = new DefaultCellEditor(editOrderBox);
		getColumnModel().getColumn(4).setCellEditor(orderEditor);
		orderEditor.addCellEditorListener(editOrderBoxListener);
	}

	public void addTableSubOrderComboBox() {
		editSubOrderBox.setPopupWidth(250);
		DefaultCellEditor subOrderEditor = new DefaultCellEditor(editSubOrderBox);
		getColumnModel().getColumn(5).setCellEditor(subOrderEditor);
	}

	public void removeNotSelectedLines() {
		int selectedRow = getSelectedRow();
		int rowCount = getModel().getRowCount();
		DefaultTableModel tableModel = (DefaultTableModel) getModel();
		for (int i = rowCount - 1; i >= 0; i--) {
			if (i != selectedRow) {
				tableModel.removeRow(i);
			}
		}
	}

	public void removeColumnsForModification() {
		for (String s : REMOVED_COLUMNS_IN_MODIFY_STATE) {
			this.removeColumn(this.getColumn(s));
		}
	}

	public void setColumnLabels() {
		tableModel.setColumnIdentifiers(COLUMN_LABELS_OVERVIEW_STATE);
	}

	public void setColumnWidth() {
		TableColumn column = null;
		DefaultTableCellRenderer tableRenderer = new DefaultTableCellRenderer();
		tableRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < getColumnCount(); i++) {
			column = getColumnModel().getColumn(i);
			column.setCellRenderer(tableRenderer);
			if (i == 0) {
				column.setPreferredWidth(1);
			} else if (i > 0 && i < 5) {
				column.setPreferredWidth(50);
			} else {
				column.setPreferredWidth(100);
			}
		}
	}

	public void setColumnWidthForModification() {
		TableColumn column = null;
		DefaultTableCellRenderer tableRenderer = new DefaultTableCellRenderer();
		tableRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < getColumnCount(); i++) {
			column = getColumnModel().getColumn(i);
			column.setCellRenderer(tableRenderer);
			if (i == 0) {
				column.setPreferredWidth(column.getPreferredWidth());
			} else {
				column.setPreferredWidth(100);
			}
		}
	}

	public MainOrderDto getSelectedMainOrder() {
		return (MainOrderDto) editOrderBox.getSelectedItem();
	}

	public SubOrderDto getSelectedSubOrder() {
		return (SubOrderDto) editSubOrderBox.getSelectedItem();
	}

	public SteppedComboBox<MainOrderDto> getEditOrderBox() {
		return editOrderBox;
	}

	public SteppedComboBox<SubOrderDto> getEditSubOrderBox() {
		return editSubOrderBox;
	}

}
