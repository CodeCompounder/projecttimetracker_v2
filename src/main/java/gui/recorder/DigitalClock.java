package gui.recorder;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;

public class DigitalClock {
    final DateFormat theClockFormat = new SimpleDateFormat("HH:mm");
    final JLabel theClockLabel = new JLabel();
    
    public DigitalClock() {
        /* a timer with delay of one second */
        final Timer clockTimer = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String text = theClockFormat.format(new Date());
                /* synchronized since threaded access */
                synchronized(theClockLabel.getTreeLock()) {
                    theClockLabel.setText(text);
                }
            }
        });
        clockTimer.start();
        setLabelDesign();
    }
    
    private void setLabelDesign() {
    	theClockLabel.setFont(new Font("Monospaced", 1, 20));
    	theClockLabel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Time"),
                BorderFactory.createBevelBorder(BevelBorder.LOWERED)));
    	theClockLabel.setOpaque(true);
    	theClockLabel.setHorizontalAlignment(JLabel.CENTER);		
	}

	public JLabel getDigitalClock() {
        return theClockLabel;
    }
}
