package gui.recorder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import org.jdatepicker.impl.JDatePickerImpl;

import common.EffortDto;
import common.MainOrderDto;
import common.OrderStatusDto;
import common.SubOrderDto;
import controller.ApplicationControllerInf;
import gui.overview.OverviewGui;
import gui.projects.ProjectsGui;
import util.DateAndTimeHelper;
import util.PttJDatePickerPanel;
import util.SteppedComboBox;

public class RecorderGui {

	private ApplicationControllerInf controller;
	private JFrame mainFrame = new JFrame("ProjectTimeTracker V2.1");
	SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");

	// Upper Panel
	private JPanel upperPanel = new JPanel();
	private PttJDatePickerPanel datePickerPanel;
	private JButton startButton = new JButton("START");
	private JButton stopButton = new JButton("STOP / Pause");
	private JButton projectsButton = new JButton("Projects");
	private JButton overviewButton = new JButton("Overview");

	// Record Panel
	private JPanel recordPanel = new JPanel(new BorderLayout());
	private JTextField startTimeField = new JTextField("", 3);
	private JTextField stopTimeField = new JTextField("", 3);
	private JTextField pauseTimeField = new JTextField("", 2);
	private JTextField searchField = new JTextField("", 9);
	private SteppedComboBox<MainOrderDto> orderComboBox = new SteppedComboBox<>();
	private JTextField catLabel = new JTextField("", 7);
	private JButton saveButton = new JButton("save");
	private SteppedComboBox<SubOrderDto> subOrderComboBox = new SteppedComboBox<>();
	private SteppedComboBox<OrderStatusDto> statusComboBox = new SteppedComboBox<>();
	private JTextField commentTextField = new JTextField("", 10);

	// Overview Panel
	private JPanel overviewPanel = new JPanel(new BorderLayout());
	private EffortTable overviewTable = new EffortTable();
	private JScrollPane scrollPane = new JScrollPane(overviewTable);
	private JButton modifyButton = new JButton("modify");
	private JButton deleteButton = new JButton("delete");
	private JButton cancelButton = new JButton("cancel");
	private JButton closeButton = new JButton("EXIT");

	// Former controller fields
	private boolean itemEventTrigger = true;
	private TableMouseAdapter tableMouseListener = new TableMouseAdapter();
	private CellEditorListener editOrderBoxListener = new EditOrderBoxListener();

	public RecorderGui(ApplicationControllerInf controller) {
		this.controller = controller;
		initUpperPanel();
		initRecordPanel();
		initOverviewPanel();
		initMainFrame();
		mainFrame.setVisible(true);

		initData();
		initActionListener();
		fillOverviewTable();
	}

	private void initActionListener() {
		addMainOrderSearchKeyListener();
		addProjectsButtonActionListener();
		addOverviewButtonActionListener();

		orderComboBox.addItemListener(new ComboBoxListener());

		addSaveButtonActionListener();
		addModifyButtonActionListener();
		addDeleteButtonActionListener();
		addCancelButtonActionListener();

		addExitButtonActionListener();
		addMainFrameWindowListener();

		addDatePickerActionListener();

		overviewTable.addMouseListener(tableMouseListener);
	}

	private void initData() {
		fillOrderComboBox();
		fillStatusComboBox();
	}

	private void fillOrderComboBox() {
		itemEventTrigger = false; // Set Flag to avoid ItemEvent of ComboBox
		orderComboBox.removeAllItems();
		List<MainOrderDto> orderList = controller.getMainOrdersByName(searchField.getText(), true);
		orderComboBox.setModel(
				new DefaultComboBoxModel<MainOrderDto>(orderList.toArray(new MainOrderDto[0])));
		itemEventTrigger = true;
		orderComboBox.setSelectedIndex(-1);
	}

	private void addMainOrderSearchKeyListener() {
		searchField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				fillOrderComboBox();
				orderComboBox.setSelectedIndex(-1);
				if (orderComboBox.getItemCount() > 0) {
					orderComboBox.setSelectedIndex(0);
				}
			}
		});
	}

	private void addProjectsButtonActionListener() {
		projectsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ProjectsGui projectsGui = new ProjectsGui(controller);
				projectsGui.getMainFrame().addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent f) {
						subOrderComboBox.setSelectedIndex(-1);
						fillOverviewTable();
						fillOrderComboBox();
					}
				});
			}
		});
	}

	private void fillOverviewTable() {
		DefaultTableModel overviewTableModel = (DefaultTableModel) overviewTable.getModel();
		overviewTableModel.setRowCount(0);
		LocalDate localDate = DateAndTimeHelper
				.getLocalDateFromDatePicker(datePickerPanel.getDatePicker());
		List<EffortDto> effortList = controller.getEffortByDay(localDate);
		for (EffortDto effort : effortList) {
			overviewTableModel.addRow(effort.getTableRow());
		}
		setBorderLabel("Overview | Total time " + controller.getTotalTime(localDate));
	}

	private void addOverviewButtonActionListener() {

		overviewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				OverviewGui overviewGui = new OverviewGui(controller);
				overviewGui.getMainFrame().addWindowListener(new WindowAdapter() {

					@Override
					public void windowClosed(WindowEvent f) {
						subOrderComboBox.setSelectedIndex(-1);
						fillOrderComboBox();
					}
				});
			}
		});
	}

	private void fillStatusComboBox() {
		List<OrderStatusDto> statusList = controller.getAllStatuses();
		statusComboBox.setModel(new DefaultComboBoxModel<OrderStatusDto>(
				statusList.toArray(new OrderStatusDto[0])));
	}

	private void addSaveButtonActionListener() {
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!selectedProjectIsValid()) {
					return;
				}
				;

				try {
					String startTime = startTimeField.getText();
					String stopTime = stopTimeField.getText();
					if (!timeInputsAreValid(startTime, stopTime)) {
						return;
					}

					String pauseTime = pauseTimeField.getText();
					if (!pauseInputIsValid(pauseTime)) {
						return;
					}

					EffortDto newEffort = createNewEffortDto();

					controller.createEffort(newEffort);
					fillOverviewTable();
					startTimeField.setText(stopTimeField.getText());
					stopTimeField.setText("");
					commentTextField.setText("");
					pauseTimeField.setText("");
				} catch (Exception f) {
					JOptionPane.showMessageDialog(mainFrame, f.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	protected EffortDto createNewEffortDto() {
		EffortDto effortDto = new EffortDto();

		effortDto.setEffortDate(
				DateAndTimeHelper.getLocalDateFromDatePicker(datePickerPanel.getDatePicker()));
		effortDto.setStartTime(DateAndTimeHelper.getLocalTimeHHMM(startTimeField.getText()));
		effortDto.setEndTime(DateAndTimeHelper.getLocalTimeHHMM(stopTimeField.getText()));
		effortDto.setPauseTime(DateAndTimeHelper.getLocalTimeMM(pauseTimeField.getText()));
		effortDto.setMainOrder((MainOrderDto) orderComboBox.getSelectedItem());
		effortDto.setSubOrder((subOrderComboBox.getSelectedIndex() == -1) ? null
				: ((SubOrderDto) subOrderComboBox.getSelectedItem()));
		effortDto.setComment(commentTextField.getText());
		effortDto.setOrderStatus((OrderStatusDto) statusComboBox.getSelectedItem());
		return effortDto;
	}

	private boolean selectedProjectIsValid() {
		if (orderComboBox.getSelectedIndex() == -1) {
			String message = "Select valid project!";
			JOptionPane.showMessageDialog(mainFrame, message, "Invalid Project",
					JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
		return true;
	}

	private boolean timeInputsAreValid(String... dateStrings) {
		for (String dateString : dateStrings) {
			if (!(DateAndTimeHelper.isTimeValidHHMM(dateString))) {
				String message = "Invalid input in Start/Stop fields. Use format hh:mm.";
				JOptionPane.showMessageDialog(mainFrame, message, "Error",
						JOptionPane.INFORMATION_MESSAGE);
				return false;
			}
		}
		return true;
	}

	protected boolean pauseInputIsValid(String pauseString) {
		if (!(DateAndTimeHelper.isTimeValidMM(pauseString))) {
			String message = "Invalid input in pause field. Use format mm.";
			JOptionPane.showMessageDialog(mainFrame, message, "Error",
					JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
		return true;
	}

	private void addModifyButtonActionListener() {
		modifyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				overviewTable.getDefaultEditor(getClass()).stopCellEditing();
				int effortId = Integer.valueOf((String) overviewTable.getValueAt(0, 0));
				EffortDto effort = controller.getEffortById(effortId);

				String startTime = (String) overviewTable.getValueAt(0, 1);
				String stopTime = (String) overviewTable.getValueAt(0, 2);
				if (!timeInputsAreValid(startTime, stopTime)) {
					return;
				}

				String pauseTime = (String) overviewTable.getValueAt(0, 3);
				if (!pauseInputIsValid(pauseTime)) {
					return;
				}

				MainOrderDto selectedMainOrder = overviewTable.getSelectedMainOrder();
				SubOrderDto selectedSubOrder = overviewTable.getSelectedSubOrder();
				String comment = (String) overviewTable.getValueAt(0, 6);

				modifyEffort(effort, startTime, stopTime, pauseTime, selectedMainOrder,
						selectedSubOrder, comment);

				controller.modifyEffort(effort);
				fillOverviewTable();
				resetOverviewTable();
			}

			private void modifyEffort(EffortDto effort, String startTime, String stopTime,
					String pauseTime, MainOrderDto selectedMainOrder, SubOrderDto selectedSubOrder,
					String comment) {
				effort.setStartTime(DateAndTimeHelper.getLocalTimeHHMM(startTime));
				effort.setEndTime(DateAndTimeHelper.getLocalTimeHHMM(stopTime));
				effort.setPauseTime(DateAndTimeHelper.getLocalTimeMM(pauseTime));
				effort.setMainOrder(selectedMainOrder);
				effort.setSubOrder(selectedSubOrder);
				effort.setComment(comment);
			}
		});
	}

	public void resetOverviewTable() {
		overviewTable.getColumnModel().getColumn(4).getCellEditor()
				.removeCellEditorListener(editOrderBoxListener);

		overviewTable.setColumnLabels();

		modifyButton.setVisible(false);
		deleteButton.setVisible(false);
		cancelButton.setVisible(false);

		overviewTable.addMouseListener(tableMouseListener);
		overviewTable.setColumnWidth();
	}

	private void addDeleteButtonActionListener() {

		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int effortId = Integer.valueOf(overviewTable.getValueAt(0, 0).toString());
				EffortDto effort = controller.getEffortById(effortId);
				int i = JOptionPane.showConfirmDialog(mainFrame, "Delete selected effort?",
						"Delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if (i == 1)
					return;
				controller.deleteEntity(effort);
				fillOverviewTable();
				resetOverviewTable();
			}
		});
	}

	private void addCancelButtonActionListener() {
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fillOverviewTable();
				resetOverviewTable();
			}
		});
	}

	private void addExitButtonActionListener() {
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}

	private void addMainFrameWindowListener() {
		mainFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	private void addDatePickerActionListener() {
		datePickerPanel.getDatePicker().getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				fillOverviewTable();
				changeDatePickerColorIfNotToday(datePickerPanel.getDatePicker());
			}
		});
	}

	protected void changeDatePickerColorIfNotToday(JDatePickerImpl jDatePickerImpl) {
		LocalDate selectedDate = DateAndTimeHelper.getLocalDateFromDatePicker(jDatePickerImpl);
		if (selectedDate.isEqual(LocalDate.now())) {
			jDatePickerImpl.getJFormattedTextField().setBackground(Color.WHITE);
		} else {
			jDatePickerImpl.getJFormattedTextField().setBackground(Color.YELLOW);
		}
	}

	private void initUpperPanel() {
		upperPanel.setLayout(new BorderLayout());

		// Date and time panel
		JPanel upperWestPanel = new JPanel(new GridLayout(2, 0, 5, 5));
		upperWestPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		// Date picker
		datePickerPanel = new PttJDatePickerPanel();
		datePickerPanel.getPickerPanel().setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Date"),
						BorderFactory.createBevelBorder(BevelBorder.LOWERED)));
		upperWestPanel.add(datePickerPanel.getPickerPanel());

		// Time panel
		final JLabel clockLabel = new DigitalClock().getDigitalClock();
		upperWestPanel.add(clockLabel);

		upperPanel.add(upperWestPanel, BorderLayout.WEST);

		// Buttons START + STOPP
		JPanel upperCenterPanel = new JPanel(new GridLayout(2, 0, 5, 5));
		upperCenterPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 0, 5, 100),
						BorderFactory.createTitledBorder("Record")));
		startButton.setBackground(Color.GREEN);
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (stopTimeField.getText().equals("")) {
					startTimeField.setText(clockLabel.getText());
					stopButton.setBackground(Color.RED);
					startButton.setBackground(Color.LIGHT_GRAY);
				} else {
					try {
						Date restartDate = timeFormatter.parse(clockLabel.getText());
						Date stopDate = timeFormatter.parse(stopTimeField.getText());
						long pauseLong = restartDate.getTime() - stopDate.getTime()
								+ ((pauseTimeField.getText().equals("")) ? 0
										: Long.parseLong(pauseTimeField.getText()));
						long pauseMinutes = pauseLong / (1000 * 60);
						pauseTimeField.setText(String.valueOf(pauseMinutes));
						stopTimeField.setText("");
						stopButton.setBackground(Color.RED);
						startButton.setBackground(Color.LIGHT_GRAY);
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
				}
				;
			}
		});
		stopButton.setBackground(Color.GRAY);
		stopButton.setForeground(Color.WHITE);
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stopTimeField.setText(clockLabel.getText());
				stopButton.setBackground(Color.GRAY);
				startButton.setBackground(Color.GREEN);
			}
		});
		upperCenterPanel.add(startButton);
		upperCenterPanel.add(stopButton);
		upperPanel.add(upperCenterPanel, BorderLayout.CENTER);

		// Buttons project / overview
		JPanel upperEastPanel = new JPanel(new GridLayout(2, 0, 5, 5));
		upperEastPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 100),
						BorderFactory.createTitledBorder("Tools")));
		upperEastPanel.add(projectsButton);
		upperEastPanel.add(overviewButton);
		upperPanel.add(upperEastPanel, BorderLayout.EAST);
	}

	private void initRecordPanel() {

		// 1. Zeit-Felder
		JPanel recordLeftPanel = new JPanel(new GridLayout(2, 0, 5, 5));
		recordLeftPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder("Time")));

		JPanel recordLeftNorthPanel = new JPanel(new GridLayout(0, 2));
		JPanel recordLeftSouthPanel = new JPanel(new GridLayout(0, 2));

		recordLeftNorthPanel.add(new JLabel("Start: "));
		recordLeftNorthPanel.add(startTimeField);
		recordLeftPanel.add(recordLeftNorthPanel);

		recordLeftSouthPanel.add(new JLabel("Stop: "));
		recordLeftSouthPanel.add(stopTimeField);
		recordLeftPanel.add(recordLeftSouthPanel);

		recordPanel.add(recordLeftPanel, BorderLayout.WEST);

		// 2. Project Felder
		JPanel recordCenterPanel = new JPanel(new BorderLayout());

		JPanel recordCenterLeftPanel = new JPanel(new GridLayout(2, 0, 5, 5));
		recordCenterLeftPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder("Project")));

		JPanel recordCenterLeftObenPanel = new JPanel(new BorderLayout());
		recordCenterLeftObenPanel.add(new JLabel("Search: "), BorderLayout.WEST);
		recordCenterLeftObenPanel.add(searchField, BorderLayout.CENTER);
		recordCenterLeftPanel.add(recordCenterLeftObenPanel);

		JPanel recordCenterLeftUntenPanel = new JPanel(new BorderLayout());
		orderComboBox.setPopupWidth(300);
		orderComboBox.setPreferredSize(new Dimension(150, 1));
		recordCenterLeftUntenPanel.add(orderComboBox, BorderLayout.WEST);
		recordCenterLeftUntenPanel.add(new JLabel(""), BorderLayout.CENTER);
		recordCenterLeftPanel.add(recordCenterLeftUntenPanel);

		recordCenterPanel.add(recordCenterLeftPanel, BorderLayout.WEST);
		recordPanel.add(recordCenterPanel, BorderLayout.CENTER);

		// 3. Save Button
		JPanel recordCenterWestPanel = new JPanel(new BorderLayout());
		recordCenterWestPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		recordCenterWestPanel.add(saveButton, BorderLayout.CENTER);
		recordCenterPanel.add(recordCenterWestPanel, BorderLayout.CENTER);

		// 3.1 Pause Field
		JPanel recordCenterEastPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		// recordCenterEastPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		recordCenterEastPanel.add(new JLabel("Pause: "));
		recordCenterEastPanel.add(pauseTimeField);
		recordCenterEastPanel.add(new JLabel("min"));
		recordCenterWestPanel.add(recordCenterEastPanel, BorderLayout.SOUTH);

		// 4. Additional Fields
		JPanel recordRightPanel = new JPanel(new BorderLayout());
		recordRightPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder("Additonal")));

		JPanel recordRightWestPanel = new JPanel(new GridLayout(2, 0, 5, 5));

		JPanel recordRightWestObenPanel = new JPanel(new BorderLayout());
		recordRightWestObenPanel.add(new JLabel("Sub/Status: "), BorderLayout.CENTER);
		subOrderComboBox.setPreferredSize(new Dimension(100, 1));
		subOrderComboBox.setPopupWidth(250);
		recordRightWestObenPanel.add(subOrderComboBox, BorderLayout.EAST);
		recordRightWestPanel.add(recordRightWestObenPanel);

		JPanel recordRightWestUntenPanel = new JPanel(new BorderLayout());
		recordRightWestUntenPanel.add(new JLabel("Comment: "), BorderLayout.WEST);
		recordRightWestUntenPanel.add(commentTextField, BorderLayout.CENTER);
		recordRightWestPanel.add(recordRightWestUntenPanel);
		recordRightPanel.add(recordRightWestPanel, BorderLayout.WEST);

		JPanel recordRightCenterPanel = new JPanel(new GridLayout(2, 0, 5, 5));
		recordRightCenterPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
		statusComboBox.setPreferredSize(new Dimension(70, 1));
		statusComboBox.setPopupWidth(120);
		recordRightCenterPanel.add(statusComboBox);
		catLabel.setEditable(false);
		recordRightCenterPanel.add(catLabel);
		recordRightPanel.add(recordRightCenterPanel, BorderLayout.CENTER);

		recordPanel.add(recordRightPanel, BorderLayout.EAST);

		recordPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5),
						BorderFactory.createTitledBorder(
								BorderFactory.createLineBorder(new Color(255, 127, 80), 2),
								"Record")));
		upperPanel.add(recordPanel, BorderLayout.SOUTH);
	}

	private void initOverviewPanel() {
		// Tabelle 'Overview'
		scrollPane.setLayout(new ScrollPaneLayout());
		overviewPanel.add(scrollPane, BorderLayout.CENTER);
		overviewPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 5, 5, 5),
						BorderFactory.createTitledBorder("Overview")));

		// SouthPanel mit Buttons
		JPanel overviewSouthPanel = new JPanel(new GridLayout(0, 8, 5, 5));
		overviewPanel.add(overviewSouthPanel, BorderLayout.SOUTH);
		overviewSouthPanel.add(modifyButton);
		modifyButton.setVisible(false);
		overviewSouthPanel.add(deleteButton);
		deleteButton.setVisible(false);
		overviewSouthPanel.add(cancelButton);
		cancelButton.setVisible(false);
		overviewSouthPanel.add(new JLabel(""));
		overviewSouthPanel.add(new JLabel(""));
		overviewSouthPanel.add(new JLabel(""));
		overviewSouthPanel.add(new JLabel(""));
		overviewSouthPanel.add(closeButton);
	}

	private void initMainFrame() {
		mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		mainFrame.setLayout(new BorderLayout());
		try {
			Image image = ImageIO.read(getClass().getResource("/icon.png"));
			mainFrame.setIconImage(image);
		} catch (IOException e) {
			System.out.println("Can't find logo!");
			e.printStackTrace();
		}

		mainFrame.add(upperPanel, BorderLayout.NORTH);
		mainFrame.add(overviewPanel, BorderLayout.CENTER);

		mainFrame.setSize(730, 500);
		mainFrame.setLocationRelativeTo(null);
	}

	public void setBorderLabel(String label) {
		overviewPanel.setBorder(
				BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 5, 5, 5),
						BorderFactory.createTitledBorder(label)));
	}

	private class TableMouseAdapter extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent e) {

			overviewTable.removeMouseListener(tableMouseListener);
			overviewTable.removeNotSelectedLines();

			overviewTable.removeColumnsForModification();
			overviewTable.setColumnWidthForModification();

			modifyButton.setVisible(true);
			deleteButton.setVisible(true);
			cancelButton.setVisible(true);

			overviewTable.addTableOrderComboBox(controller.getAllMainOrders(true),
					editOrderBoxListener);
			overviewTable.addTableSubOrderComboBox();

			EffortDto effort = getSelectedEffort();

			overviewTable.getEditOrderBox().setSelectedItem(effort.getMainOrder());

			if (effort.getSubOrder() != null) {
				overviewTable.getEditSubOrderBox().setSelectedItem(effort.getSubOrder());
			}
		}

		private EffortDto getSelectedEffort() {
			int effortId = Integer.valueOf(overviewTable.getValueAt(0, 0).toString());
			return controller.getEffortById(effortId);
		}
	}

	private class ComboBoxListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				// If event is triggered by .addItem operation -> return
				if (itemEventTrigger == false) {
					return;
				} else {
					subOrderComboBox.removeAllItems();
					int index = orderComboBox.getSelectedIndex();
					if (index == -1)
						return;
					MainOrderDto mainOrder = (MainOrderDto) orderComboBox.getSelectedItem();
					List<SubOrderDto> subOrderList = controller.getSubOrders(mainOrder, true);
					subOrderComboBox.setModel(new DefaultComboBoxModel<SubOrderDto>(
							subOrderList.toArray(new SubOrderDto[0])));

					catLabel.setText((mainOrder.getOrderCategory() == null) ? ""
							: mainOrder.getOrderCategory());
				}
			}
		}
	}

	private class EditOrderBoxListener implements CellEditorListener {

		@Override
		public void editingStopped(ChangeEvent e) {
			overviewTable.getEditSubOrderBox().removeAllItems();
			MainOrderDto mainOrder = (MainOrderDto) overviewTable.getEditOrderBox()
					.getSelectedItem();
			List<SubOrderDto> subOrderList = controller.getSubOrders(mainOrder, true);
			overviewTable.getEditSubOrderBox().setModel(new DefaultComboBoxModel<SubOrderDto>(
					subOrderList.toArray(new SubOrderDto[0])));
		}

		@Override
		public void editingCanceled(ChangeEvent e) {
			// No action
		}
	}
}
