package persistance.queryresults;

import java.time.LocalDate;

import persistance.entities.MainOrder;
import persistance.entities.OrderStatus;
import persistance.entities.SubOrder;

public class GroupedEffort {

	LocalDate startDate;
	LocalDate endDate;
	private int summedTime;
	private MainOrder mainOrder;
	private SubOrder subOrder;
	private OrderStatus status;

	private final static int SUMMED_TIME_INDEX = 0;
	private final static int MAINORDER_INDEX = 1;
	private final static int SUBORDER_INDEX = 2;
	private final static int STATUS_INDEX = 3;

	public GroupedEffort(Object[] queryResult, LocalDate startDate, LocalDate endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.summedTime = (int) queryResult[SUMMED_TIME_INDEX];
		this.mainOrder = (MainOrder) queryResult[MAINORDER_INDEX];
		this.subOrder = (SubOrder) queryResult[SUBORDER_INDEX];
		this.status = (OrderStatus) queryResult[STATUS_INDEX];
	}

	public GroupedEffort(LocalDate startDate, LocalDate endDate, int summedTime,
			MainOrder mainOrder, SubOrder subOrder, OrderStatus status) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.summedTime = summedTime;
		this.mainOrder = mainOrder;
		this.subOrder = subOrder;
		this.status = status;
	}

	public int getSummedTime() {
		return summedTime;
	}

	public MainOrder getMainOrder() {
		return mainOrder;
	}

	public SubOrder getSubOrder() {
		return subOrder;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

}
