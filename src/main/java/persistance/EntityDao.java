package persistance;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;

import common.MainOrderDto;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import persistance.entities.Effort;
import persistance.entities.MainOrder;
import persistance.entities.OrderCategory;
import persistance.entities.OrderStatus;
import persistance.entities.ProjectTimeTrackerEntityInf;
import persistance.entities.SubOrder;
import persistance.queryresults.GroupedEffort;
import util.DateAndTimeHelper;

public class EntityDao {

	private Session session = HibernateSessionUtil.getSessionFactory().openSession();

	public ProjectTimeTrackerEntityInf insertEntity(ProjectTimeTrackerEntityInf entity) {
		session.beginTransaction();
		session.persist(entity);
		// Flush/Clear/Refresh necessary to get content of calculated field 'duration'
		session.flush();
		session.clear();
		session.refresh(entity);
		session.getTransaction().commit();
		return entity;
	}

	public void updateEntity(ProjectTimeTrackerEntityInf entity) {
		session.beginTransaction();
		session.merge(entity);
		// Flush/Clear/Refresh necessary to get content of calculated field 'duration'
		session.flush();
		session.clear();
		session.refresh(entity);
		session.getTransaction().commit();
	}

	public List<MainOrder> getMainOrders(boolean onlyVisible) {
		return (getMainOrdersByName(null, onlyVisible));
	}

	public MainOrder getMainOrderById(Integer mainOrderId) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<MainOrder> cr = cb.createQuery(MainOrder.class);
		Root<MainOrder> root = cr.from(MainOrder.class);
		cr.select(root).where(cb.equal(root.get("id"), mainOrderId));

		Query<MainOrder> query = session.createQuery(cr);
		return query.getSingleResult();
	}

	public SubOrder getSubOrderById(Integer subOrderId) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<SubOrder> cr = cb.createQuery(SubOrder.class);
		Root<SubOrder> root = cr.from(SubOrder.class);
		cr.select(root).where(cb.equal(root.get("id"), subOrderId));

		Query<SubOrder> query = session.createQuery(cr);
		return query.getSingleResult();
	}

	public List<MainOrder> getMainOrdersByName(String orderName, boolean onlyVisible) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<MainOrder> cr = cb.createQuery(MainOrder.class);
		Root<MainOrder> root = cr.from(MainOrder.class);

		List<Predicate> predicateList = new ArrayList<>();

		if (orderName != null) {
			predicateList.add(cb.ilike(root.<String>get("mainOrderName"), "%" + orderName + "%"));
		}

		if (onlyVisible) {
			predicateList.add(cb.isTrue(root.<Boolean>get("visible")));
		}

		if (!predicateList.isEmpty()) {
			cr.select(root).where(predicateList.toArray(Predicate[]::new));
		} else {
			cr.select(root);
		}

		cr.orderBy(cb.asc(root.<String>get("mainOrderNumber")),
				cb.asc(root.<String>get("mainOrderName")));

		Query<MainOrder> query = session.createQuery(cr);
		return query.getResultList();
	}

	public void deleteEntity(Object entity) {
		session.beginTransaction();
		session.remove(entity);
		session.getTransaction().commit();
	}

	public void deleteEntity(Class<? extends ProjectTimeTrackerEntityInf> entityClass, Integer id) {
		ProjectTimeTrackerEntityInf entityToBeDeleted = session.find(entityClass, id);
		deleteEntity(entityToBeDeleted);
	}

	public List<OrderCategory> getCategories() {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<OrderCategory> cr = cb.createQuery(OrderCategory.class);
		Root<OrderCategory> root = cr.from(OrderCategory.class);
		cr.select(root);

		Query<OrderCategory> query = session.createQuery(cr);
		return query.getResultList();
	}

	public OrderCategory getCategory(String categoryName) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<OrderCategory> cr = cb.createQuery(OrderCategory.class);
		Root<OrderCategory> root = cr.from(OrderCategory.class);
		cr.select(root).where(cb.like(root.<String>get("categoryName"), categoryName));

		Query<OrderCategory> query = session.createQuery(cr);
		return query.getSingleResult();
	}

	public OrderStatus getStatusByName(String statusName) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<OrderStatus> cr = cb.createQuery(OrderStatus.class);
		Root<OrderStatus> root = cr.from(OrderStatus.class);
		cr.select(root).where(cb.equal(root.get("statusName"), statusName));

		Query<OrderStatus> query = session.createQuery(cr);
		return query.getSingleResult();
	}

	public List<OrderStatus> getStatuses() {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<OrderStatus> cr = cb.createQuery(OrderStatus.class);
		Root<OrderStatus> root = cr.from(OrderStatus.class);
		cr.select(root);

		Query<OrderStatus> query = session.createQuery(cr);
		return query.getResultList();
	}

	public List<SubOrder> getSubOrders(MainOrderDto mainOrderDto) {
		return getSubOrders(mainOrderDto, false);
	}

	public List<SubOrder> getSubOrders(MainOrderDto mainOrderDto, boolean onlyVisible) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<SubOrder> cr = cb.createQuery(SubOrder.class);
		Root<SubOrder> root = cr.from(SubOrder.class);

		List<Predicate> predicateList = new ArrayList<>();

		predicateList.add(cb.equal(root.get("mainOrder").get("id"), mainOrderDto.getId()));

		if (onlyVisible) {
			predicateList.add(cb.isTrue(root.<Boolean>get("visible")));
		}

		cr.select(root).where(predicateList.toArray(Predicate[]::new));

		Query<SubOrder> query = session.createQuery(cr);
		return query.getResultList();
	}

	public List<Effort> getEffortByDay(LocalDate date) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Effort> cr = cb.createQuery(Effort.class);
		Root<Effort> root = cr.from(Effort.class);
		cr.select(root).where(cb.equal(root.get("effortDate"), date));

		Query<Effort> query = session.createQuery(cr);
		return query.getResultList();
	}

	public List<GroupedEffort> getGroupedEffortByDate(LocalDate startDate, LocalDate endDate,
			String categoryName, String statusName) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Object[]> cr = cb.createQuery(Object[].class);
		Root<Effort> root = cr.from(Effort.class);

		List<Predicate> predicateList = new ArrayList<>();

		predicateList.add(cb.between(root.get("effortDate"), startDate, endDate));

		if (statusName != null) {
			predicateList.add(cb.equal(root.get("status").get("statusName"), statusName));
		}

		if (categoryName != null) {
			predicateList.add(cb.equal(root.get("mainOrder").get("category").get("categoryName"),
					categoryName));
		}

		cr.multiselect(cb.sum(root.get("duration")), root.get("mainOrder"),
				root.join("subOrder", JoinType.LEFT), root.join("status", JoinType.LEFT));
		cr.where(predicateList.toArray(Predicate[]::new));
		cr.groupBy(root.get("mainOrder"), root.get("subOrder"), root.get("status"));

		Query<Object[]> query = session.createQuery(cr);

		List<Object[]> queryResults = (query.getResultList());

		List<GroupedEffort> groupedResults = new ArrayList<>();
		for (Object[] object : queryResults) {
			groupedResults.add(new GroupedEffort(object, startDate, endDate));
		}
		return groupedResults;
	}

	public LocalTime getTotalTime(LocalDate date) {
		List<Effort> effortList = getEffortByDay(date);
		Long totalTime = 0L;
		for (Effort effort : effortList) {
			totalTime = totalTime + effort.getDuration();
		}
		return DateAndTimeHelper.getLocalTimeMM(String.valueOf(totalTime));
	}

	public Effort getEffortById(int effortId) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Effort> cr = cb.createQuery(Effort.class);
		Root<Effort> root = cr.from(Effort.class);
		cr.select(root).where(cb.equal(root.get("id"), effortId));

		Query<Effort> query = session.createQuery(cr);
		return query.getSingleResult();
	}

	public List<Effort> getEfforts(GroupedEffort groupedEffort) {
		HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Effort> cr = cb.createQuery(Effort.class);
		Root<Effort> root = cr.from(Effort.class);

		List<Predicate> predicateList = new ArrayList<>();

		predicateList.add(cb.between(root.get("effortDate"), groupedEffort.getStartDate(),
				groupedEffort.getEndDate()));

		predicateList.add(cb.equal(root.get("mainOrder"), groupedEffort.getMainOrder()));

		if (groupedEffort.getSubOrder() != null) {
			predicateList.add(cb.equal(root.get("subOrder"), groupedEffort.getSubOrder()));
		} else {
			predicateList.add(cb.isNull(root.get("subOrder")));
		}

		if (groupedEffort.getStatus() != null) {
			predicateList.add(cb.equal(root.get("status"), groupedEffort.getStatus()));
		}

		cr.select(root).where(predicateList.toArray(Predicate[]::new));

		Query<Effort> query = session.createQuery(cr);
		return query.getResultList();
	}
}
