package persistance;

public class PropertyNotSetException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PropertyNotSetException(String message) {
		super(message);
	}
}
