package persistance.entities;

public interface ProjectTimeTrackerEntityInf {

	public String getSignature();
	
	public Integer getId();
}
