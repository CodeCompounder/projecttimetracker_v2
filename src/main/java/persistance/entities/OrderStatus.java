package persistance.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "order_statuses")
public class OrderStatus implements ProjectTimeTrackerEntityInf{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	Integer id;
    
    @Column(unique = true, nullable = false)
	String statusName;

    public OrderStatus() {}
    
	public OrderStatus(String statusName) {
		super();
		this.statusName = statusName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String toString() {
		return getStatusName();
	}
	
	@Override
	public String getSignature() {
		return getStatusName();
	}
}
