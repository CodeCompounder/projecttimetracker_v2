package persistance.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(name = "suborders", 
	uniqueConstraints = {@UniqueConstraint(columnNames = {"subOrderNumber", "subOrderName"})})
public class SubOrder implements Comparable<SubOrder>, ProjectTimeTrackerEntityInf {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
	@Column(nullable = false)
    private String subOrderNumber;
	
	@Column(nullable = false)
    private String subOrderName;
    
	@Column(nullable = false)
    private Boolean visible;
    
    @ManyToOne
    @JoinColumn(name = "mainOrder_id")
    private MainOrder mainOrder;

    public SubOrder() {}
    
    public SubOrder(String subOrderName, String subOrderNumber, boolean visible, MainOrder mainOrder) {
    	this.subOrderName = subOrderName;
    	this.subOrderNumber = subOrderNumber;
    	this.visible = visible;
    	this.mainOrder = mainOrder;
    }

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubOrderNumber() {
		return subOrderNumber;
	}

	public void setSubOrderNumber(String subOrderNumber) {
		this.subOrderNumber = subOrderNumber;
	}

	public String getSubOrderName() {
		return subOrderName;
	}

	public void setSubOrderName(String subOrderName) {
		this.subOrderName = subOrderName;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	
	public String toString() {
		String objectString = (subOrderNumber + " | " + subOrderName);
		if (visible == false) {
			objectString = (objectString + " | " + "NOT VISIBLE");
		}
		return objectString;
	}

	public MainOrder getMainOrder() {
		return mainOrder;
	}

	public void setMainOrder(MainOrder mainOrder) {
		this.mainOrder = mainOrder;
	}

	@Override
	public int compareTo(SubOrder o) {
		return subOrderNumber.compareTo(o.getSubOrderNumber());
	}

	@Override
	public String getSignature() {
		return subOrderNumber + subOrderName + mainOrder.getId() + visible;
	}
}
