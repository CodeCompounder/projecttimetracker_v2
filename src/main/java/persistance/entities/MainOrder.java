package persistance.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(name = "mainorders", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "mainOrderNumber", "mainOrderName" }) })
public class MainOrder implements Comparable<MainOrder>, ProjectTimeTrackerEntityInf {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private String mainOrderNumber;

	@Column(nullable = false)
	private String mainOrderName;

	@ManyToOne
	@JoinColumn(name = "orderCategory_id")
	private OrderCategory category;

	@Column(nullable = false)
	private Boolean visible;

	public MainOrder() {}

	public MainOrder(String mainOrderNumber, String mainOrderName, OrderCategory category, boolean visible) {
		super();
		this.mainOrderNumber = mainOrderNumber;
		this.mainOrderName = mainOrderName;
		this.category = category;
		this.visible = visible;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getMainOrderNumber() {
		return mainOrderNumber;
	}

	public void setMainOrderNumber(String mainOrderNumber) {
		this.mainOrderNumber = mainOrderNumber;
	}

	public String getMainOrderName() {
		return mainOrderName;
	}

	public void setMainOrderName(String mainOrderName) {
		this.mainOrderName = mainOrderName;
	}

	public OrderCategory getCategory() {
		return category;
	}

	public void setCategory(OrderCategory category) {
		this.category = category;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public String toString() {
		String objectString = (mainOrderNumber + " | " + mainOrderName);
		if (visible == false) {
			objectString = (objectString + " | " + "NOT VISIBLE");
		}
		return objectString;
	}

	@Override
	public int compareTo(MainOrder o) {
		return (mainOrderNumber.compareTo(o.getMainOrderNumber()));
	}

	public String getSignature() {
		if (category != null) {
			return mainOrderNumber + mainOrderName + category.toString() + visible;			
		} else {
			return mainOrderNumber + mainOrderName + visible;
		}
	}
}
