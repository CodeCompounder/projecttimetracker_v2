package persistance.entities;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoField;

import org.hibernate.annotations.Formula;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import persistance.PropertyNotSetException;
import util.DateAndTimeHelper;

@Entity
@Table(name = "efforts")
public class Effort implements ProjectTimeTrackerEntityInf {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private LocalDate effortDate;

	@Column(nullable = false)
	private LocalTime startTime;

	@Column(nullable = false)
	private LocalTime endTime;

	private LocalTime pauseTime;

	@Formula("CASE WHEN "
			+ "(datediff(Minute, dateadd(Minute, datediff(Minute, TIME '00:00:00', pausetime), starttime), endtime)) < 0 "
			+ "THEN ((datediff(Minute, dateadd(Minute, datediff(Minute, TIME '00:00:00', pausetime), starttime), endtime)) + 24*60) "
			+ "ELSE (datediff(Minute, dateadd(Minute, datediff(Minute, TIME '00:00:00', pausetime), starttime), endtime)) END")
	private Integer duration;

	@ManyToOne(optional = false)
	@JoinColumn(name = "mainOrder_id")
	private MainOrder mainOrder;

	@ManyToOne(optional = true)
	@JoinColumn(name = "subOrder_id")
	private SubOrder subOrder;

	@ManyToOne(optional = true)
	@JoinColumn(name = "status_id")
	private OrderStatus status;

	private String comment;

	public Effort() {
	}

	public Effort(LocalDate day, LocalTime startTime, LocalTime endTime, LocalTime pauseTime, MainOrder mainOrder,
			SubOrder subOrder, OrderStatus status, String comment) {
		super();
		this.effortDate = day;
		this.startTime = startTime;
		this.endTime = endTime;
		this.pauseTime = pauseTime;
		this.mainOrder = mainOrder;
		this.subOrder = subOrder;
		this.status = status;
		this.comment = comment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getEffortDate() {
		return effortDate;
	}

	public void setEffortDate(LocalDate effortDate) {
		this.effortDate = effortDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public LocalTime getPauseTime() {
		return pauseTime;
	}

	public void setPauseTime(LocalTime pauseTime) {
		this.pauseTime = pauseTime;
	}

	public MainOrder getMainOrder() {
		return mainOrder;
	}

	public void setMainOrder(MainOrder mainOrder) {
		this.mainOrder = mainOrder;
	}

	public SubOrder getSubOrder() {
		return subOrder;
	}

	public void setSubOrder(SubOrder subOrder) {
		this.subOrder = subOrder;
	}

	public OrderStatus getStatus() throws PropertyNotSetException {
		if (status != null) {
			return status;			
		}
		throw new PropertyNotSetException("OrderStatus");
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String getSignature() {
		return effortDate.toString() + startTime + endTime + pauseTime;
	}

	// ColumnLabels: {"ID", "Start", "Stop", "Pause", "[min]", "OrderNr", "Project",
	// "Category", "SubOrder", "SubProject", "Status", "Comment"};
	public String[] getTableRow() {
		String[] tableRow = new String[12];
		tableRow[0] = id.toString();
		tableRow[1] = startTime.toString();
		tableRow[2] = endTime.toString();
		tableRow[3] = String.valueOf(pauseTime.get(ChronoField.MINUTE_OF_DAY));
		tableRow[4] = DateAndTimeHelper.getLocalTimeMM(getDuration()).toString();
		tableRow[5] = mainOrder.getMainOrderNumber();
		tableRow[6] = mainOrder.getMainOrderName();
		tableRow[7] = (mainOrder.getCategory() == null) ? "" : mainOrder.getCategory().getCategoryName();
		tableRow[8] = (subOrder == null) ? "" : subOrder.getSubOrderNumber();
		tableRow[9] = (subOrder == null) ? "" : subOrder.getSubOrderName();
		tableRow[10] = (status == null) ? "" : status.toString();
		tableRow[11] = comment;
		return tableRow;
	}

	public Integer getDuration() {
		return duration;
	}
}
