package main;

import javax.swing.SwingUtilities;

public class Main {

	public static void main(String[] args) {
		System.out.println("Load ProjectTimeTracker...");

		SplashScreen splashScreen = new SplashScreen();
		
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	new PttStartupManager();
            }
        });
        
        splashScreen.close();
	}
}
