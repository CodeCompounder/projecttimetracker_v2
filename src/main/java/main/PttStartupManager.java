package main;

import java.io.File;

import controller.ApplicationController;
import gui.recorder.RecorderGui;
import persistance.EntityDao;
import persistance.entities.OrderCategory;
import persistance.entities.OrderStatus;

public class PttStartupManager {

	public PttStartupManager() {

		if (applicationIsStartedFirstTime()) {
			createBasicData();
		}

		new RecorderGui(new ApplicationController());
	}

	private void createBasicData() {
		System.out.println("Inital start of Application: Create new Database...");

		EntityDao effortDao = new EntityDao();

		OrderCategory initialCategory = new OrderCategory("Default Category");
		OrderStatus initialStatus = new OrderStatus("Default Status");
		effortDao.insertEntity(initialCategory);
		effortDao.insertEntity(initialStatus);
	}

	private boolean applicationIsStartedFirstTime() {

		String currentDir = System.getProperty("user.dir");

		if (new File(currentDir + "\\ptt_database.mv.db").isFile()) {
			return false;
		}
		return true;
	}
}
