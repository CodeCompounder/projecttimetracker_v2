package main;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

public class SplashScreen {

	JFrame frame;
	Image image;
	JLabel text = new JLabel("ProjectTimeTracker V2.1");
	JLabel text2 = new JLabel("LOADING...");
	JLabel text3 = new JLabel("www.CodeCompounder.de / info@CodeCompounder.de");

	public SplashScreen() {
		createGUI();
		addText();
		addImage();
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private void addText() {
		text.setFont(new Font("arial", Font.BOLD, 20));
		text.setHorizontalAlignment(JTextField.CENTER);
		frame.add(text, BorderLayout.NORTH);

		JPanel southPanel = new JPanel(new BorderLayout());

		text2.setFont(new Font("arial", Font.BOLD, 15));
		text2.setHorizontalAlignment(JTextField.CENTER);
		southPanel.add(text2, BorderLayout.NORTH);

		text3.setFont(new Font("arial", Font.PLAIN, 10));
		text3.setHorizontalAlignment(JTextField.CENTER);
		southPanel.add(text3, BorderLayout.SOUTH);

		frame.add(southPanel, BorderLayout.SOUTH);
	}

	private void createGUI() {
		frame = new JFrame();
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setUndecorated(true);
		frame.getRootPane().setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
	}

	private void addImage() {
		try {
			image = ImageIO.read(getClass().getResource("/SplashScreenImage_V4.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JLabel imageLabel = new JLabel(new ImageIcon(image));
		frame.add(imageLabel, BorderLayout.CENTER);

	}

	public void close() {
		frame.dispose();
	}
}
