package util;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.regex.Pattern;

import org.jdatepicker.impl.JDatePickerImpl;

public class DateAndTimeHelper {

	private static final Pattern TIME_PATTERN_HHMM = Pattern.compile("^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
	private static final Pattern TIME_PATTERN_MM = Pattern.compile("^[0-9]?[0-9]?$");
	
    public static boolean isTimeValidHHMM(String time) {    	
    	return TIME_PATTERN_HHMM.matcher(time).find();
    }
    
    public static boolean isTimeValidMM(String time) {    	
    	return TIME_PATTERN_MM.matcher(time).find();
    }
    
    public static LocalTime getLocalTimeHHMM(String timeString) {
    	DateTimeFormatter parser = DateTimeFormatter.ofPattern("HH:mm");
    	return LocalTime.parse(timeString, parser);
    }

    public static LocalTime getLocalTimeMM(int minutes) {
    	int hours = minutes/60;
    	minutes = minutes-(hours*60);
		return LocalTime.of(hours, minutes);
    }
    
    public static String getDurationStringMMtoHHMM(int totalMinutes) {
    	int hours = totalMinutes/60;
    	String hoursString = String.valueOf(hours);
    	if (hoursString.length() == 1) {
    		hoursString = "0" + hoursString;
    	}
    	
    	int minutes = totalMinutes-(hours*60);
    	String minutesString = String.valueOf(minutes);
    	if (minutesString.length() == 1) {
    		minutesString = "0" + minutesString;
    	}
		return hoursString + ":" + minutesString;
    }
    
    public static LocalTime getLocalTimeMM(String timeString) {
    	if (timeString.equals("")) {
    		timeString = "0";
    	}
    	return getLocalTimeMM(Integer.valueOf(timeString));
    }
    
	public static LocalDate getLocalDateFromDatePicker(JDatePickerImpl datePicker) {
		Date date = (Date) datePicker.getModel().getValue();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
}
