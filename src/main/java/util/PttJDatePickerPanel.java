package util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.plaf.basic.BasicArrowButton;

import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

public class PttJDatePickerPanel {

	JPanel pickerPanel = new JPanel(new BorderLayout());
	UtilDateModel dateModel;
	JDatePickerImpl datePicker;
	BasicArrowButton dateUpButton;
	BasicArrowButton dateDownButton;
	
	public PttJDatePickerPanel() {
		createDateModelToday();
		createJDatePicker();
		createPickerPanel();
	}

	private void createPickerPanel() {
        JPanel buttonPanel = new JPanel(new BorderLayout());
        
        dateUpButton = new BasicArrowButton(BasicArrowButton.NORTH);
        buttonPanel.add(dateUpButton, BorderLayout.NORTH);
        addDayChangeListener(dateUpButton, -1);
        
        dateDownButton = new BasicArrowButton(BasicArrowButton.SOUTH);
        buttonPanel.add(dateDownButton, BorderLayout.SOUTH);
        addDayChangeListener(dateDownButton, 1);
        
        pickerPanel.add(datePicker, BorderLayout.CENTER);
        pickerPanel.add(buttonPanel, BorderLayout.EAST);
	}
	
    private void addDayChangeListener(BasicArrowButton arrowButton, int daysToChange) {
    	arrowButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				datePicker.getModel().setDay(datePicker.getModel().getDay() + daysToChange);
				datePicker.getModel().setSelected(true);
			}
		});
	}


	private void createJDatePicker() {
        
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        
        JDatePanelImpl datePanel = new JDatePanelImpl(dateModel, p);
        datePicker = new JDatePickerImpl(datePanel, new DateComponentFormatter());
        datePicker.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        JFormattedTextField dateTextField = datePicker.getJFormattedTextField();
        dateTextField.setFont(new Font("Monospaced", 1, 20));
        dateTextField.setBackground(Color.WHITE);
        dateTextField.setHorizontalAlignment(JTextField.CENTER);	
	}

	private void createDateModelToday() {
		dateModel = new UtilDateModel();
        Calendar calendar  = Calendar.getInstance();
        dateModel.setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dateModel.setSelected(true);
	}
	
	public JPanel getPickerPanel() {
		return pickerPanel;
	}
	
	public JDatePickerImpl getDatePicker() {
		return datePicker;
	}	
}
