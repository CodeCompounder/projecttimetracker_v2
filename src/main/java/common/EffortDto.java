package common;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoField;

import util.DateAndTimeHelper;

public class EffortDto extends ProjectTimeTrackerDto {

	private LocalDate effortDate;
	private LocalTime startTime;
	private LocalTime endTime;
	private LocalTime pauseTime;
	private Integer duration;
	private MainOrderDto mainOrder;
	private SubOrderDto subOrder;
	private OrderStatusDto orderStatus;
	private String comment;

	public LocalDate getEffortDate() {
		return effortDate;
	}

	public void setEffortDate(LocalDate effortDate) {
		this.effortDate = effortDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public LocalTime getPauseTime() {
		return pauseTime;
	}

	public void setPauseTime(LocalTime pauseTime) {
		this.pauseTime = pauseTime;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public MainOrderDto getMainOrder() {
		return mainOrder;
	}

	public void setMainOrder(MainOrderDto mainOrderDto) {
		this.mainOrder = mainOrderDto;
	}

	public SubOrderDto getSubOrder() {
		return subOrder;
	}

	public void setSubOrder(SubOrderDto subOrderDto) {
		this.subOrder = subOrderDto;
	}

	public OrderStatusDto getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatusDto orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	// ColumnLabels: {"ID", "Start", "Stop", "Pause", "[min]", "OrderNr", "Project",
	// "Category", "SubOrder", "SubProject", "Status", "Comment"};
	public String[] getTableRow() {
		String[] tableRow = new String[12];
		tableRow[0] = id.toString();
		tableRow[1] = startTime.toString();
		tableRow[2] = endTime.toString();
		tableRow[3] = String.valueOf(pauseTime.get(ChronoField.MINUTE_OF_DAY));
		tableRow[4] = DateAndTimeHelper.getLocalTimeMM(getDuration()).toString();
		tableRow[5] = mainOrder.getMainOrderNumber();
		tableRow[6] = mainOrder.getName();
		tableRow[7] = (mainOrder.getOrderCategory() == null) ? "" : mainOrder.getOrderCategory();
		tableRow[8] = (subOrder == null) ? "" : subOrder.getSubOrderNumber();
		tableRow[9] = (subOrder == null) ? "" : subOrder.getName();
		tableRow[10] = (orderStatus == null) ? "" : orderStatus.toString();
		tableRow[11] = comment;
		return tableRow;
	}
}
