package common;

import java.time.LocalDate;

public class GroupedEffortDto {

	private LocalDate startDate;
	private LocalDate endDate;
	private int summedTime;
	private MainOrderDto mainOrder;
	private SubOrderDto subOrder;
	private OrderStatusDto status;

	public GroupedEffortDto(LocalDate startDate, LocalDate endDate, int summedTime,
			MainOrderDto mainOrder, SubOrderDto subOrder, OrderStatusDto status) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.summedTime = summedTime;
		this.mainOrder = mainOrder;
		this.subOrder = subOrder;
		this.status = status;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public int getSummedTime() {
		return summedTime;
	}

	public MainOrderDto getMainOrder() {
		return mainOrder;
	}

	public SubOrderDto getSubOrder() {
		return subOrder;
	}

	public OrderStatusDto getStatus() {
		return status;
	}
}
