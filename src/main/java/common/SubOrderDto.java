package common;

public class SubOrderDto extends ProjectTimeTrackerDto {

	private String subOrderNumber;
	private boolean visible;
	private Integer mainOrderId;

	public SubOrderDto() {
	}

	public SubOrderDto(Builder builder) {
		this.id = builder.id;
		this.subOrderNumber = builder.subOrderNumber;
		this.name = builder.name;
		this.mainOrderId = builder.mainOrderId;
		this.visible = builder.visible;
	}

	public String getSubOrderNumber() {
		return subOrderNumber;
	}

	public void setSubOrderNumber(String subOrderNumber) {
		this.subOrderNumber = subOrderNumber;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public Integer getMainOrderId() {
		return mainOrderId;
	}

	public void setMainOrderId(Integer mainOrderId) {
		this.mainOrderId = mainOrderId;
	};

	@Override
	public String toString() {
		String dtoToString = (subOrderNumber + " | " + name);
		if (visible == false) {
			dtoToString = (dtoToString + " | " + "NOT VISIBLE");
		}
		return dtoToString;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null || !(object instanceof SubOrderDto)) {
			return false;
		}

		return id == ((SubOrderDto) object).getId();
	}

	public static class Builder {

		private Integer id;
		private String subOrderNumber;
		private String name;
		private boolean visible;
		private Integer mainOrderId;

		public Builder() {
		}

		public Builder withId(Integer id) {
			this.id = id;
			return this;
		}

		public Builder withSubOrderNumber(String subOrderNumber) {
			this.subOrderNumber = subOrderNumber;
			return this;
		}

		public Builder withSubOrderName(String subOrderName) {
			this.name = subOrderName;
			return this;
		}

		public Builder withMainOrderId(Integer mainOrderId) {
			this.mainOrderId = mainOrderId;
			return this;
		}

		public Builder withVisible(boolean visible) {
			this.visible = visible;
			return this;
		}

		public SubOrderDto build() {
			return new SubOrderDto(this);
		}
	};
}
