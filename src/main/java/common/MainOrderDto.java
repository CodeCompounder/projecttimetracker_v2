package common;

public class MainOrderDto extends ProjectTimeTrackerDto implements Comparable<MainOrderDto> {

	private String mainOrderNumber;
	private String orderCategory;
	private boolean visible;

	public MainOrderDto() {
	}

	public MainOrderDto(Builder builder) {
		this.id = builder.id;
		this.mainOrderNumber = builder.mainOrderNumber;
		this.name = builder.name;
		this.orderCategory = builder.orderCategory;
		this.visible = builder.visible;
	}

	public String getMainOrderNumber() {
		return mainOrderNumber;
	}

	public void setMainOrderNumber(String mainOrderNumber) {
		this.mainOrderNumber = mainOrderNumber;
	}

	public String getOrderCategory() {
		return orderCategory;
	}

	public void setOrderCategory(String orderCategory) {
		this.orderCategory = orderCategory;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public static class Builder {

		private Integer id;
		private String mainOrderNumber;
		private String name;
		private String orderCategory;
		private boolean visible;

		public Builder() {
		};

		public Builder withId(Integer id) {
			this.id = id;
			return this;
		}

		public Builder withMainOrderNumber(String mainOrderNumber) {
			this.mainOrderNumber = mainOrderNumber;
			return this;
		}

		public Builder withMainOrderName(String mainOrderName) {
			this.name = mainOrderName;
			return this;
		}

		public Builder withOrderCategery(String orderCategory) {
			this.orderCategory = orderCategory;
			return this;
		}

		public Builder withVisible(boolean visible) {
			this.visible = visible;
			return this;
		}

		public MainOrderDto build() {
			return new MainOrderDto(this);
		}
	}

	@Override
	public int compareTo(MainOrderDto mainOrderDto) {
		int result = mainOrderNumber.compareTo(mainOrderDto.getMainOrderNumber());
		if (result == 0) {
			result = name.compareTo(mainOrderDto.getName());
		}
		return result;
	}

	@Override
	public String toString() {
		String dtoToString = (mainOrderNumber + " | " + name);
		if (visible == false) {
			dtoToString = (dtoToString + " | " + "NOT VISIBLE");
		}
		return dtoToString;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null || !(object instanceof MainOrderDto)) {
			return false;
		}

		return id == ((MainOrderDto) object).getId();
	}
}
