package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

import common.EffortDto;
import common.GroupedEffortDto;
import common.MainOrderDto;
import common.OrderCategoryDto;
import common.OrderStatusDto;
import common.ProjectTimeTrackerDto;
import common.SubOrderDto;
import persistance.EntityDao;
import persistance.entities.Effort;
import persistance.entities.MainOrder;
import persistance.entities.OrderCategory;
import persistance.entities.OrderStatus;
import persistance.entities.ProjectTimeTrackerEntityInf;
import persistance.entities.SubOrder;
import persistance.queryresults.GroupedEffort;

public class ApplicationController implements ApplicationControllerInf {

	private EntityDao entityDao = new EntityDao();
	private EntityDtoMapper mapper = new EntityDtoMapper();

	@Override
	public List<MainOrderDto> getAllMainOrders(boolean onlyVisible) {
		List<MainOrder> allMainOrders = entityDao.getMainOrders(onlyVisible);
		return allMainOrders.stream().map(m -> mapper.mainOrderEntityToDto(m))
				.collect(Collectors.toList());
	}

	@Override
	public List<OrderCategoryDto> getAllCategories() {
		List<OrderCategory> allCategories = entityDao.getCategories();
		return allCategories.stream().map(m -> mapper.categoryEntityToDto(m))
				.collect(Collectors.toList());
	}

	@Override
	public List<OrderStatusDto> getAllStatuses() {
		List<OrderStatus> allStatuses = entityDao.getStatuses();
		return allStatuses.stream().map(m -> mapper.statusEntityToDto(m))
				.collect(Collectors.toList());
	}

	@Override
	public List<SubOrderDto> getSubOrders(MainOrderDto mainOrderDto, boolean onlyVisible) {
		List<SubOrder> subOrders = entityDao.getSubOrders(mainOrderDto, onlyVisible);
		return subOrders.stream().map(m -> mapper.subOrderEntityToDto(m))
				.collect(Collectors.toList());
	}

	@Override
	public List<MainOrderDto> getMainOrdersByName(String namePattern, boolean onlyVisible) {
		List<MainOrder> mainOrders = entityDao.getMainOrdersByName(namePattern, onlyVisible);
		return mainOrders.stream().map(m -> mapper.mainOrderEntityToDto(m))
				.collect(Collectors.toList());
	}

	@Override
	public List<EffortDto> getEffortByDay(LocalDate localDate) {
		List<Effort> efforts = entityDao.getEffortByDay(localDate);
		return efforts.stream().map(e -> mapper.effortEntityToDto(e)).collect(Collectors.toList());
	}

	@Override
	public EffortDto getEffortById(Integer id) {
		Effort effort = entityDao.getEffortById(id);
		return mapper.effortEntityToDto(effort);
	}

	@Override
	public void createMainOrder(MainOrderDto newMainOrderDto) {
		OrderCategory category = entityDao.getCategory(newMainOrderDto.getOrderCategory());
		MainOrder newMainOrder = mapper.mainOrderDtoToEntity(newMainOrderDto, category);
		ProjectTimeTrackerEntityInf persistedMainOrder = entityDao.insertEntity(newMainOrder);
		newMainOrderDto.setId(persistedMainOrder.getId());
	}

	@Override
	public void createSubOrder(SubOrderDto newSubOrderDto) {
		MainOrder mainOrder = entityDao.getMainOrderById(newSubOrderDto.getMainOrderId());
		SubOrder newSubOrder = mapper.subOrderDtoToEntity(newSubOrderDto, mainOrder);
		entityDao.insertEntity(newSubOrder);
	}

	@Override
	public void createCategory(String newCategory) {
		OrderCategory category = new OrderCategory(newCategory);
		entityDao.insertEntity(category);
	}

	@Override
	public void createOrderStatus(String newOrderStatus) {
		OrderStatus status = new OrderStatus(newOrderStatus);
		entityDao.insertEntity(status);
	}

	@Override
	public void createEffort(EffortDto effortDto) {
		OrderStatus status = entityDao.getStatusByName(effortDto.getOrderStatus().getName());
		MainOrder mainOrder = entityDao.getMainOrderById(effortDto.getMainOrder().getId());

		SubOrderDto subOrderDto = effortDto.getSubOrder();
		SubOrder subOrder = null;
		if (subOrderDto != null) {
			subOrder = entityDao.getSubOrderById(subOrderDto.getId());
		}

		Effort newEffort = mapper.effortDtoToEntity(effortDto, mainOrder, subOrder, status);

		ProjectTimeTrackerEntityInf persistedMainOrder = entityDao.insertEntity(newEffort);
		effortDto.setId(persistedMainOrder.getId());
	}

	@Override
	public void modifyMainOrder(MainOrderDto mainOrderDto) {
		OrderCategory category = entityDao.getCategory(mainOrderDto.getOrderCategory());
		MainOrder mainOrder = mapper.mainOrderDtoToEntity(mainOrderDto, category);
		entityDao.updateEntity(mainOrder);
	}

	@Override
	public void modifySubOrder(SubOrderDto subOrderDto) {
		MainOrder mainOrder = entityDao.getMainOrderById(subOrderDto.getMainOrderId());
		SubOrder subOrder = mapper.subOrderDtoToEntity(subOrderDto, mainOrder);
		entityDao.updateEntity(subOrder);
	}

	@Override
	public void modifyCategory(OrderCategoryDto categoryDto) {
		OrderCategory category = mapper.categoryDtoToEntity(categoryDto);
		entityDao.updateEntity(category);
	}

	@Override
	public void modifyOrderStatus(OrderStatusDto orderStatusDto) {
		OrderStatus status = mapper.statusDtoToEntity(orderStatusDto);
		entityDao.updateEntity(status);
	}

	@Override
	public void modifyEffort(EffortDto effortDto) {
		MainOrder mainOrder = entityDao.getMainOrderById(effortDto.getMainOrder().getId());

		SubOrderDto subOrderDto = effortDto.getSubOrder();
		SubOrder subOrder = null;
		if (subOrderDto != null) {
			subOrder = entityDao.getSubOrderById(subOrderDto.getId());
		}

		OrderStatus status = entityDao.getStatusByName(effortDto.getOrderStatus().getName());
		Effort effort = mapper.effortDtoToEntity(effortDto, mainOrder, subOrder, status);
		entityDao.updateEntity(effort);
	}

	@Override
	public void deleteEntity(ProjectTimeTrackerDto aDto) {
		entityDao.deleteEntity(mapper.getEntityClass(aDto), aDto.getId());
	}

	@Override
	public LocalTime getTotalTime(LocalDate date) {
		return entityDao.getTotalTime(date);
	}

	@Override
	public List<GroupedEffortDto> getGroupedEffortByDate(LocalDate startDate, LocalDate endDate,
			OrderCategoryDto category, OrderStatusDto status) {
		String categoryName = category != null ? category.getName() : null;
		String statusName = status != null ? status.getName() : null;

		List<GroupedEffort> groupedEfforts = entityDao.getGroupedEffortByDate(startDate, endDate,
				categoryName, statusName);
		return groupedEfforts.stream().map(g -> mapper.groupedEffortToDto(g))
				.collect(Collectors.toList());
	}

	@Override
	public List<EffortDto> getEfforts(GroupedEffortDto groupedEffortDto) {
		OrderCategory category = entityDao
				.getCategory(groupedEffortDto.getMainOrder().getOrderCategory());
		MainOrder mainOrder = mapper.mainOrderDtoToEntity(groupedEffortDto.getMainOrder(),
				category);

		SubOrderDto subOrderDto = groupedEffortDto.getSubOrder();
		SubOrder subOrder = null;
		if (subOrderDto != null) {
			subOrder = mapper.subOrderDtoToEntity(subOrderDto, mainOrder);
		}

		OrderStatus status = mapper.statusDtoToEntity(groupedEffortDto.getStatus());

		GroupedEffort groupedEffort = new GroupedEffort(groupedEffortDto.getStartDate(),
				groupedEffortDto.getEndDate(), groupedEffortDto.getSummedTime(), mainOrder,
				subOrder, status);

		List<Effort> efforts = entityDao.getEfforts(groupedEffort);
		return efforts.stream().map(e -> mapper.effortEntityToDto(e)).collect(Collectors.toList());
	}
}
