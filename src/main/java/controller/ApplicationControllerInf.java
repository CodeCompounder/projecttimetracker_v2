package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import common.EffortDto;
import common.GroupedEffortDto;
import common.MainOrderDto;
import common.OrderCategoryDto;
import common.OrderStatusDto;
import common.ProjectTimeTrackerDto;
import common.SubOrderDto;

public interface ApplicationControllerInf {

	public List<MainOrderDto> getAllMainOrders(boolean onlyVisible);

	public List<OrderCategoryDto> getAllCategories();

	public List<OrderStatusDto> getAllStatuses();

	public List<SubOrderDto> getSubOrders(MainOrderDto mainOrderDto, boolean onlyVisible);

	public List<MainOrderDto> getMainOrdersByName(String namePattern, boolean onlyVisible);

	public List<EffortDto> getEffortByDay(LocalDate localDate);

	public EffortDto getEffortById(Integer id);

	public void createMainOrder(MainOrderDto newMainOrderDto);

	public void createSubOrder(SubOrderDto newSubOrderDto);

	public void createCategory(String newCategory);

	public void createOrderStatus(String newOrderStatus);

	public void createEffort(EffortDto effortDto);

	public void modifyMainOrder(MainOrderDto mainOrderDto);

	public void modifySubOrder(SubOrderDto subOrderDto);

	public void modifyCategory(OrderCategoryDto categoryDto);

	public void modifyOrderStatus(OrderStatusDto orderStatusDto);

	public void modifyEffort(EffortDto effortDto);

	public void deleteEntity(ProjectTimeTrackerDto aDto);

	public LocalTime getTotalTime(LocalDate date);

	public List<GroupedEffortDto> getGroupedEffortByDate(LocalDate startDate, LocalDate endDate,
			OrderCategoryDto category, OrderStatusDto status);

	public List<EffortDto> getEfforts(GroupedEffortDto groupedEffortDto);
}
