package controller;

import java.text.MessageFormat;

import common.EffortDto;
import common.GroupedEffortDto;
import common.MainOrderDto;
import common.OrderCategoryDto;
import common.OrderStatusDto;
import common.ProjectTimeTrackerDto;
import common.SubOrderDto;
import persistance.PropertyNotSetException;
import persistance.entities.Effort;
import persistance.entities.MainOrder;
import persistance.entities.OrderCategory;
import persistance.entities.OrderStatus;
import persistance.entities.ProjectTimeTrackerEntityInf;
import persistance.entities.SubOrder;
import persistance.queryresults.GroupedEffort;

public class EntityDtoMapper {

	public MainOrder mainOrderDtoToEntity(MainOrderDto mainOrderDto, OrderCategory category) {
		MainOrder mainOrder = new MainOrder();
		mainOrder.setId(mainOrderDto.getId());
		mainOrder.setMainOrderName(mainOrderDto.getName());
		mainOrder.setMainOrderNumber(mainOrderDto.getMainOrderNumber());
		mainOrder.setCategory(category);
		mainOrder.setVisible(mainOrderDto.isVisible());
		return mainOrder;
	}

	public MainOrderDto mainOrderEntityToDto(MainOrder mainOrder) {
		return new MainOrderDto.Builder().withId(mainOrder.getId())
				.withMainOrderNumber(mainOrder.getMainOrderNumber())
				.withMainOrderName(mainOrder.getMainOrderName())
				.withOrderCategery(mainOrder.getCategory().getCategoryName())
				.withVisible(mainOrder.getVisible()).build();
	}

	public SubOrder subOrderDtoToEntity(SubOrderDto subOrderDto, MainOrder mainOrder) {
		SubOrder subOrder = new SubOrder();
		subOrder.setId(subOrderDto.getId());
		subOrder.setSubOrderName(subOrderDto.getName());
		subOrder.setSubOrderNumber(subOrderDto.getSubOrderNumber());
		subOrder.setMainOrder(mainOrder);
		;
		subOrder.setVisible(subOrderDto.isVisible());
		return subOrder;
	}

	public SubOrderDto subOrderEntityToDto(SubOrder subOrder) {
		return new SubOrderDto.Builder().withId(subOrder.getId())
				.withSubOrderNumber(subOrder.getSubOrderNumber())
				.withSubOrderName(subOrder.getSubOrderName())
				.withMainOrderId(subOrder.getMainOrder().getId()).withVisible(subOrder.getVisible())
				.build();
	}

	public OrderCategory categoryDtoToEntity(OrderCategoryDto categoryDto) {
		OrderCategory category = new OrderCategory();
		category.setId(categoryDto.getId());
		category.setCategoryName(categoryDto.getName());
		return category;
	}

	public OrderCategoryDto categoryEntityToDto(OrderCategory category) {
		OrderCategoryDto dto = new OrderCategoryDto();
		dto.setName(category.getCategoryName());
		dto.setId(Integer.valueOf(category.getId()));
		return dto;
	}

	public OrderStatus statusDtoToEntity(OrderStatusDto statusDto) {
		OrderStatus status = new OrderStatus();
		status.setId(statusDto.getId());
		status.setStatusName(statusDto.getName());
		return status;
	}

	public OrderStatusDto statusEntityToDto(OrderStatus aStatus) {
		OrderStatusDto dto = new OrderStatusDto();
		dto.setId(Integer.valueOf(aStatus.getId()));
		dto.setName(aStatus.getStatusName());
		return dto;
	}

	public Effort effortDtoToEntity(EffortDto effortDto, MainOrder mainOrder, SubOrder subOrder,
			OrderStatus status) {
		Effort effort = new Effort();
		effort.setId(effortDto.getId());
		effort.setStartTime(effortDto.getStartTime());
		effort.setEndTime(effortDto.getEndTime());
		effort.setPauseTime(effortDto.getPauseTime());
		effort.setEffortDate(effortDto.getEffortDate());
		effort.setComment(effortDto.getComment());
		effort.setStatus(status);
		effort.setMainOrder(mainOrder);
		effort.setSubOrder(subOrder);
		return effort;
	}

	public EffortDto effortEntityToDto(Effort effort) {
		EffortDto dto = new EffortDto();
		dto.setId(effort.getId());
		dto.setName(effort.getSignature());
		dto.setStartTime(effort.getStartTime());
		dto.setEndTime(effort.getEndTime());
		dto.setEffortDate(effort.getEffortDate());
		dto.setDuration(effort.getDuration());
		dto.setMainOrder(mainOrderEntityToDto(effort.getMainOrder()));

		if (effort.getSubOrder() != null) {
			dto.setSubOrder(subOrderEntityToDto(effort.getSubOrder()));
		}

		dto.setComment(effort.getComment());
		dto.setPauseTime(effort.getPauseTime());
		try {
			dto.setOrderStatus(statusEntityToDto(effort.getStatus()));
		} catch (PropertyNotSetException e) {
			dto.setOrderStatus(null);
			e.printStackTrace();
		}
		return dto;
	}

	public Class<? extends ProjectTimeTrackerEntityInf> getEntityClass(ProjectTimeTrackerDto aDto) {

		if (aDto instanceof MainOrderDto) {
			return MainOrder.class;
		} else if (aDto instanceof SubOrderDto) {
			return SubOrder.class;
		} else if (aDto instanceof EffortDto) {
			return Effort.class;
		} else if (aDto instanceof OrderCategoryDto) {
			return OrderCategory.class;
		} else if (aDto instanceof OrderStatusDto) {
			return OrderStatus.class;
		} else {
			throw new IllegalStateException(
					MessageFormat.format("DTO type unknwon: {0}", aDto.getClass()));
		}
	}

	public GroupedEffortDto groupedEffortToDto(GroupedEffort groupedEffort) {
		MainOrderDto mainOrderDto = mainOrderEntityToDto(groupedEffort.getMainOrder());

		SubOrder subOrder = groupedEffort.getSubOrder();
		SubOrderDto subOrderDto = null;
		if (subOrder != null) {
			subOrderDto = subOrderEntityToDto(subOrder);
		}
		OrderStatusDto status = statusEntityToDto(groupedEffort.getStatus());

		return new GroupedEffortDto(groupedEffort.getStartDate(), groupedEffort.getEndDate(),
				groupedEffort.getSummedTime(), mainOrderDto, subOrderDto, status);
	}
}
