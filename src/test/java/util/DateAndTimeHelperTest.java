package util;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class DateAndTimeHelperTest {

	@ParameterizedTest
	@ValueSource(strings = {"00:00", "23:59", "12:00", "09:30", "01:20"})
	void testIsTimeValidForValidTimesHHMM(String validTime) {
		assertTrue(DateAndTimeHelper.isTimeValidHHMM(validTime));		
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"0:0", "24:00", "9:3", "09:60", "000:30", "12:3", "9:3o", "9:30", ""})
	void testIsTimeValidForInvalidTimesHHMM(String validTime) {
		assertFalse(DateAndTimeHelper.isTimeValidHHMM(validTime));		
	}

	@Test
	void testGetLocalTimeHHMM() {
		LocalTime localTime = DateAndTimeHelper.getLocalTimeHHMM("09:30");
		assertEquals(localTime.getHour(), 9);
		assertEquals(localTime.getMinute(), 30);
		assertEquals(localTime.getSecond(), 0);
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"00", "59", "50", "1", "99", ""})
	void testIsTimeValidForValidTimesMM(String validTime) {
		assertTrue(DateAndTimeHelper.isTimeValidMM(validTime));		
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"000", "100"})
	void testIsTimeValidForInvalidTimesMM(String validTime) {
		assertFalse(DateAndTimeHelper.isTimeValidMM(validTime));		
	}

	@Test
	void testGetLocalTimeMM() {
		LocalTime localTime = DateAndTimeHelper.getLocalTimeMM("99");
		assertEquals(localTime.getHour(), 1);
		assertEquals(localTime.getMinute(), 39);
		assertEquals(localTime.getSecond(), 0);
		
		localTime = DateAndTimeHelper.getLocalTimeMM("60");
		assertEquals(localTime.getHour(), 1);
		assertEquals(localTime.getMinute(), 0);
		assertEquals(localTime.getSecond(), 0);
		
		localTime = DateAndTimeHelper.getLocalTimeMM("59");
		assertEquals(localTime.getHour(), 0);
		assertEquals(localTime.getMinute(), 59);
		assertEquals(localTime.getSecond(), 0);
		
		localTime = DateAndTimeHelper.getLocalTimeMM("");
		assertEquals(localTime.getHour(), 0);
		assertEquals(localTime.getMinute(), 0);
		assertEquals(localTime.getSecond(), 0);
	}

}
