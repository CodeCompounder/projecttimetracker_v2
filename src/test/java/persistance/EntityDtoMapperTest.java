package persistance;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

import common.EffortDto;
import common.MainOrderDto;
import common.OrderCategoryDto;
import common.OrderStatusDto;
import common.SubOrderDto;
import controller.EntityDtoMapper;
import persistance.entities.Effort;
import persistance.entities.MainOrder;
import persistance.entities.OrderCategory;
import persistance.entities.OrderStatus;
import persistance.entities.SubOrder;

class EntityDtoMapperTest {

	EntityDtoMapper entityDtoMapper = new EntityDtoMapper();

	@Test
	void testEffortDtoToEntity() throws PropertyNotSetException {
		MainOrderDto mainOrderDto = new MainOrderDto.Builder().withId(2).build();
		SubOrderDto subOrderDto = new SubOrderDto.Builder().withId(3).build();
		OrderStatusDto statusDto = new OrderStatusDto();
		statusDto.setName("status name");

		EffortDto dto = new EffortDto();
		dto.setId(1);
		dto.setName("2022-12-2612:0014:3000:15");
		dto.setStartTime(LocalTime.of(12, 0));
		dto.setEndTime(LocalTime.of(14, 30));
		dto.setPauseTime(LocalTime.of(0, 15));
		dto.setDuration(135);
		dto.setOrderStatus(statusDto);
		dto.setComment("comment");
		dto.setMainOrder(mainOrderDto);
		dto.setSubOrder(subOrderDto);
		dto.setEffortDate(LocalDate.of(2022, 12, 26));

		MainOrder mainOrder = new MainOrder();
		mainOrder.setId(2);

		SubOrder subOrder = new SubOrder();
		subOrder.setId(3);

		OrderStatus status = new OrderStatus();
		status.setStatusName("status name");

		Effort entity = entityDtoMapper.effortDtoToEntity(dto, mainOrder, subOrder, status);

		assertEquals(dto.getId(), entity.getId());
		assertEquals(dto.getName(), entity.getSignature());
		assertEquals(dto.getStartTime(), entity.getStartTime());
		assertEquals(dto.getEndTime(), entity.getEndTime());
		assertEquals(dto.getEffortDate(), entity.getEffortDate());
		assertEquals(null, entity.getDuration());
		assertEquals(dto.getPauseTime(), entity.getPauseTime());
		assertEquals(dto.getOrderStatus().getName(), entity.getStatus().getStatusName());
		assertEquals(dto.getMainOrder().getId(), entity.getMainOrder().getId());
		assertEquals(dto.getSubOrder().getId(), entity.getSubOrder().getId());
		assertEquals(dto.getComment(), entity.getComment());
	}

	@Test
	void testEffortEntityToDto() throws PropertyNotSetException {
		MainOrder mainOrder = new MainOrder();
		mainOrder.setId(2);
		mainOrder.setCategory(new OrderCategory());
		mainOrder.setVisible(true);

		SubOrder subOrder = new SubOrder();
		subOrder.setId(3);
		subOrder.setVisible(true);
		subOrder.setMainOrder(mainOrder);

		OrderStatus status = new OrderStatus();
		status.setStatusName("status name");
		status.setId(4);

		Effort entity = new Effort();
		entity.setId(1);
		entity.setStartTime(LocalTime.of(12, 0));
		entity.setEndTime(LocalTime.of(14, 30));
		entity.setPauseTime(LocalTime.of(0, 15));
		entity.setStatus(status);
		entity.setComment("comment");
		entity.setMainOrder(mainOrder);
		entity.setSubOrder(subOrder);
		entity.setEffortDate(LocalDate.of(2022, 12, 26));

		EffortDto dto = entityDtoMapper.effortEntityToDto(entity);

		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getSignature(), dto.getName());
		assertEquals(entity.getStartTime(), dto.getStartTime());
		assertEquals(entity.getEndTime(), dto.getEndTime());
		assertEquals(entity.getEffortDate(), dto.getEffortDate());
		assertEquals(entity.getDuration(), dto.getDuration());
		assertEquals(entity.getPauseTime(), dto.getPauseTime());
		assertEquals(entity.getStatus().getStatusName(), dto.getOrderStatus().getName());
		assertEquals(entity.getMainOrder().getId(), dto.getMainOrder().getId());
		assertEquals(entity.getSubOrder().getId(), dto.getSubOrder().getId());
		assertEquals(entity.getComment(), dto.getComment());
	}

	@Test
	void testMainOrderDtoToEntity() {
		OrderCategory category = new OrderCategory();
		category.setCategoryName("category name");

		MainOrderDto dto = new MainOrderDto.Builder().withId(1).withMainOrderName("name")
				.withMainOrderNumber("number").withOrderCategery(category.getCategoryName())
				.withVisible(true).build();

		MainOrder entity = entityDtoMapper.mainOrderDtoToEntity(dto, category);

		assertEquals(dto.getId(), entity.getId());
		assertEquals(dto.getName(), entity.getMainOrderName());
		assertEquals(dto.getMainOrderNumber(), entity.getMainOrderNumber());
		assertEquals(dto.getOrderCategory(), entity.getCategory().getCategoryName());
		assertEquals(dto.isVisible(), entity.getVisible());
	}

	@Test
	void testMainOrderEntityToDto() {
		OrderCategory category = new OrderCategory();
		category.setCategoryName("category name");

		MainOrder entity = new MainOrder();
		entity.setId(1);
		entity.setMainOrderName("name");
		entity.setMainOrderNumber("number");
		entity.setCategory(category);
		entity.setVisible(true);

		MainOrderDto dto = entityDtoMapper.mainOrderEntityToDto(entity);

		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getMainOrderName(), dto.getName());
		assertEquals(entity.getMainOrderNumber(), dto.getMainOrderNumber());
		assertEquals(entity.getCategory().getCategoryName(), dto.getOrderCategory());
		assertEquals(entity.getVisible(), dto.isVisible());
	}

	@Test
	void testSubOrderDtoToEntity() {
		MainOrder mainOrder = new MainOrder();
		mainOrder.setId(2);

		SubOrderDto dto = new SubOrderDto.Builder().withId(1).withSubOrderName("name")
				.withSubOrderNumber("number").withMainOrderId(mainOrder.getId()).withVisible(true)
				.build();

		SubOrder entity = entityDtoMapper.subOrderDtoToEntity(dto, mainOrder);

		assertEquals(dto.getId(), entity.getId());
		assertEquals(dto.getName(), entity.getSubOrderName());
		assertEquals(dto.getSubOrderNumber(), entity.getSubOrderNumber());
		assertEquals(dto.getMainOrderId(), entity.getMainOrder().getId());
		assertEquals(dto.isVisible(), entity.getVisible());
	}

	@Test
	void testSubOrderEntityToDto() {
		MainOrder mainOrder = new MainOrder();
		mainOrder.setId(2);

		SubOrder entity = new SubOrder();
		entity.setId(1);
		entity.setSubOrderName("name");
		entity.setSubOrderNumber("number");
		entity.setMainOrder(mainOrder);
		entity.setVisible(true);

		SubOrderDto dto = entityDtoMapper.subOrderEntityToDto(entity);

		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getSubOrderName(), dto.getName());
		assertEquals(entity.getSubOrderNumber(), dto.getSubOrderNumber());
		assertEquals(entity.getMainOrder().getId(), dto.getMainOrderId());
		assertEquals(entity.getVisible(), dto.isVisible());
	}

	@Test
	void testCategoryDtoToEntity() {
		OrderCategoryDto dto = new OrderCategoryDto();
		dto.setId(1);
		dto.setName("name");

		OrderCategory entity = entityDtoMapper.categoryDtoToEntity(dto);

		assertEquals(dto.getId(), entity.getId());
		assertEquals(dto.getName(), entity.getCategoryName());
	}

	@Test
	void testCategoryEntityToDto() {
		OrderCategory entity = new OrderCategory();
		entity.setId(1);
		entity.setCategoryName("name");

		OrderCategoryDto dto = entityDtoMapper.categoryEntityToDto(entity);

		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getCategoryName(), entity.getCategoryName());
	}

	@Test
	void testStatusDtoToEntity() {
		OrderStatusDto dto = new OrderStatusDto();
		dto.setId(1);
		dto.setName("name");

		OrderStatus entity = entityDtoMapper.statusDtoToEntity(dto);

		assertEquals(dto.getId(), entity.getId());
		assertEquals(dto.getName(), entity.getStatusName());
	}

	@Test
	void testStatusEntityToDto() {
		OrderStatus entity = new OrderStatus();
		entity.setId(1);
		entity.setStatusName("name");

		OrderStatusDto dto = entityDtoMapper.statusEntityToDto(entity);

		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getStatusName(), entity.getStatusName());
	}

	@Test
	void testGetEntityClass() {
		assertEquals(MainOrder.class, entityDtoMapper.getEntityClass(new MainOrderDto()));
		assertEquals(SubOrder.class, entityDtoMapper.getEntityClass(new SubOrderDto()));
		assertEquals(OrderCategory.class, entityDtoMapper.getEntityClass(new OrderCategoryDto()));
		assertEquals(OrderStatus.class, entityDtoMapper.getEntityClass(new OrderStatusDto()));
		assertEquals(Effort.class, entityDtoMapper.getEntityClass(new EffortDto()));
	}

}
